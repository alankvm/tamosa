<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Crear extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('reporteria_model');
        $this->load->library('pdf');
    }

    public function index()
    {
        
    }

    public function reporte()
    {
        
        $tipo = $this->input->post('crear');
        if ( isset( $tipo ) && $tipo != "" && $tipo == "pdf") {
            $this->pdf();
        } else {
            $this->excel();
        }
    }
    public function pdf()
    {
        //Nombre del archivo 
        $filename = 'rpt_'.date('dmY').'_'.substr(uniqid(md5(rand()), true), 0, 7);
        //Guardar PDF creado en proyecto/temp/{nombre}
        $ruta_archivo_pdf = APPPATH."cache/pdf/$filename.pdf";

        //Verificar si el archivo ya existe 
        if ( file_exists( $ruta_archivo_pdf ) == false )
        {
            //Incrementar limite de memoria de proceso
            ini_set( 'memory_limit', '64M' );
            
            //Adquirir id de consulta 
            $id_consulta = $this->input->post('id_consulta');
            $filtro = $this->input->post('pdf_filtros');
            //Obtener datos y construir secciones del reporte
            $data['table_header']     = $this->reporteria_model->getNombreColumnas( $id_consulta );
            $data['table_tbody']      = $this->reporteria_model->getResultadoConsultaReporte( $id_consulta, $filtro );
            $data['detalles_reporte'] = $this->reporteria_model->getDetallesReporte( $id_consulta );
            $data['listado_filtros']  = $this->reporteria_model->getListadoFiltrosConsulta( $id_consulta );
            
            //Llamar vista y guardar el resultado en HTML
            $html = $this->load->view('estadistica/crear/pdf.php', $data, true);
            $cabecera = '<table>';
            $cabecera .= '<tr>';
            $cabecera .= '<td><img src="' . base_url('/media/images/reporte/logo_small2.jpg') . '" alt="UTE" /></td>';
            $cabecera .= '<td><h1>Unidad T&eacute;cnica Ejecutiva del Sector de Justicia</h1></td>';
            $cabecera .= '</tr>';
            $cabecera .= '</table>';

            $metadata = $data['detalles_reporte'];
            
            //Nueva instancia del creador de PDF
            $pdf = $this->pdf->cargarPdf();
            $pdf->mirrorMargins = 1;
            //Agregar metadata al PDF
            $pdf->SetTitle( $metadata->que_nombre );
            $pdf->SetAuthor( $this->config->item('sistema') . ' desarrollado por Grupo Satelite El Salvador');
            //Obtener el contenido de print.css
            $stylesheet = file_get_contents( base_url( 'stylesheets/print.css' ) );
            //Importante! el parametro entero 1 indica que esta es una hoja de estilo para el reporte
            $pdf->WriteHTML( $stylesheet, 1 );
            //$pdf->SetHTMLHeader($cabecera);
            //Crear footer del reporte {sistema} - {Numero Pagina} - {fecha y hora}  
            $pdf->SetFooter( $this->config->item('sistema') . '|{PAGENO}|' . date("d/m/Y h:i:s") ); 
            //Escribir el resultado HTML en el PDF
            $pdf->WriteHTML( $html );
            //Guardar el archivo 
            $pdf->Output( $ruta_archivo_pdf, 'D'); 
        }
        
        //Mostrar PDF creado 
        //redirect($ruta_archivo_pdf);
    }

    public function excel()
    {
        //Adquirir id de consulta 
        $id_consulta = $this->input->post('id_consulta');
        $filtro = $this->input->post('pdf_filtros');
        //Nombre del archivo 
        $filename = 'rpt_'.date('dmY').'_'.substr(uniqid(md5(rand()), true), 0, 7);
        //Obtener datos y construir secciones del reporte
        $data['table_header']     = $this->reporteria_model->getNombreColumnas( $id_consulta );
        $data['table_tbody']      = $this->reporteria_model->getResultadoConsultaReporte( $id_consulta, $filtro );
        $data['detalles_reporte'] = $this->reporteria_model->getDetallesReporte( $id_consulta );
        $data['listado_filtros']  = $this->reporteria_model->getListadoFiltrosConsulta( $id_consulta );
        $data['filename']         = $filename;
        $this->load->view('estadistica/crear/excel.php', $data);
    }

}

/* End of file crear.php */
/* Location: ./application/controllers/crear.php */