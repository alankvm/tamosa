<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Query extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        //Do your magic here!
        $this->load->model('reporteria_model');
        $this->load->model('proceso_model');
    }

    public function index()
    {
        if (!$this->tank_auth->is_logged_in()) {
            redirect('/auth/login/');
        } else {
            //$this->output->enable_profiler();
            $data['listado_tablas'] = $this->reporteria_model->getListadoTablas();
            $this->_cargarvista( null, $data );
        }
    }
    
    /**
     * Obtiene columnas pertenecientes a una tabla en especifico
     */
    public function getColumnas()
    {
        //Verificar acceso unicamente, mediante AJAX
        if ( !$this->input->is_ajax_request() ) {
            exit('No direct script access allowed');
        }

        $nombre_tabla = $this->input->get('tabla');
        $result = $this->proceso_model->getColumnas( $nombre_tabla );
        echo $result;
    }

    /**
     * Almacena consulta generada 
     */
    public function nuevo()
    {
        $resultado = $this->reporteria_model->nuevoReporte();
        redirect( base_url( 'estadistica/query?resultado='.$resultado ) );
    }

    function _cargarvista($data=0,$crud=0)
    {   
        $this->load->view('vacia',$crud);   
        if($data!=0)
            $data=array_merge($data,$this->masterpage->getUsuario());
        else
            $data=$this->masterpage->getUsuario();
        $vista=$data['modulo'].'/'.$data['control'].'/'.$data['funcion'];
        $this->masterpage->setMasterPage('masterpage_default');
        $this->masterpage->addContentPage($vista, 'content',$data);
        $this->masterpage->show();
    }

}

/* End of file query.php */
/* Location: ./application/controllers/query.php */