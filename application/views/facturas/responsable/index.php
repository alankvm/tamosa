<a href="<?=base_url('facturas/lista')?>" class="btn btn-danger" style="position: relative; z-index: 1000; float: right; margin-right: 30%;">Volver</a>
<a href="<?=base_url('facturas/salida')?>" class="btn btn-primary" style="position: absolute; right: 2%; z-index: 1000; top: 17.5%">Asignar Tipo(s) de Salida(s)</a>

<table cellpadding="0" cellspacing="0" border="0" class="responsive dynamicTable display table table-bordered" width="100%">
	<thead>
		<tr>
			<th>
				Id
			</th>
			<th>
				Documento
			</th>	
			<th>
				Cliente
			</th>
			<th>
				Valor
			</th>
            <th>
                Responsable
            </th>
			<th>
				Acciones
			</th>
		</tr>
	</thead>		
	<tbody>		
		<?php foreach ($facturas as $key) { ?>
		<tr>
			<td>
				<?php echo $key['fac_id']?>
			</td>				
			<td>
				<?php echo $key['documento']?>
			</td>
			<td>
				<?php echo $key['cliente']?>
			</td>
			<td>
				<?php echo $key['valor']?>
			</td>
            <td>
                <?php echo $key['res_nombre'].' '.$key['res_apellido']?>
            </td>
			<td>
                <input type="checkbox" class="tip" value="<?php echo $key['fac_id'] ?>" name="pedidos[]" title="Asignar Responsable">
                
                <a href="#" data-id="<?=$key['fac_id']?>" name="regresar[]"><span aria-hidden="true" class="icomoon-icon-cancel tip" title="Regresar a Cola"></span>&nbsp;</a> 
				<a data-toggle="modal" href="#historial" data-id="<?php echo $key['fac_id'] ?>" onclick="cargar_historial(<?php echo $key['fac_id'] ?>)" title="Ver historial"><span aria-hidden="true" class="minia-icon-calendar-2"></span></a>
			</td>
		</tr>
		<?php } ?>
	</tbody>
</table>
<div class="row"></div>
<div class="row">
    <form class="form" id="elementos" method="POST">
        <div class="form-row">
            <div id="seleccionados">
                
            </div>
        </div>
        <select name="responsable" id="responsable" class="nostyle" style="width:50%; position: relative; float: right; bottom: -20px; margin-right: 26%;">
            <?php foreach ($responsables as $key) {?>
                <option value="<?php echo $key['res_id']?>"><?php echo $key['res_nombre'].' '.$key['res_apellido']?></option>
            <?php } ?>
        </select>
        <div class="form-actions">
            <button class="btn btn-primary" type="submit">Asignar reponsable</button>
        </div>
        
    </form>
</div>
<div class="modal fade hide" id="asignar">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h3>Asignar Responsable</h3>
    </div>
    <div class="modal-body">
    	<div class="box">            
            <div class="content">               
                <form class="form-horizontal" method="POST">                    
                    <div class="form-row row-fluid">
                        <div class="span12">
                            <div class="row-fluid">
                                <label class="form-label span4" for="nombres">Responsable</label>
                                <div class="span8 controls">  
                                    <select class="span8 text" id="responsable" name="responsable">
                                        <?php foreach ($responsables as $key) { ?>
                                            <option value="<?php echo $key['res_id']?>"><?php echo $key['res_nombre'].' '.$key['res_apellido']?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
 					<div class="form-row row-fluid">
                        <div class="span12">
                            <div class="row-fluid">
                                <label class="form-label span4" for="nombres">Observaciones</label>
                                <textarea class="span8 text" id="obs" type="text" name="obs"></textarea>
                            </div>
                        </div>
                    </div>					
					<div class="form-actions">
                        <input type="hidden" id="fac_id" name="fac_id">
                       <button type="submit" class="btn btn-info">Guardar</button>
                       <button type="button" class="btn">Cancelar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal">Cerrar</a>
    </div>
</div>

<div class="modal fade hide" id="historial">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h3>Historial</h3>
    </div>
    <div class="modal-body">
        <div class="box">
            <div class="content">               
                <div id="tabla_historial"></div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal">Cerrar</a>
    </div>
</div>
<script src="<?php echo base_url("js/facturas/responsable.js"); ?>"></script>