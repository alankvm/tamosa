<div class="row-fluid">

    <div class="span12">
        <div class="box closed">

            <div class="title">

                <h4>
                    <span class="icon16 icomoon-icon-equalizer-2"></span>
                    <span>Filtros para: <?=$detalles_reporte->que_nombre?></span>
                </h4>
                <a href="#" class="minimize" style="display: none;">Minimize</a>
            </div>
            <div class="content" style="display: block;">
                <form class="horizontal">

                    <div class="form-row row-fluid">
                        <div class="span12">
                            <div class="row-fluid">
                                <label class="form-label span3" for="checkboxes">Filtros Disponibles</label>
                                <div class="span6 controls">   
                                    <select name="filtros" id="filtros">
                                <?php   if ( $listado_filtros ):
                                            foreach ($listado_filtros->result() as $filtro): ?>
                                            <option value="<?=$filtro->fil_campo?>"><?=$filtro->fil_alias?></option>
                                <?php       endforeach;
                                        else: ?>

                                <?php   endif; ?>
                                    </select>
                                </div> 
                            </div>
                        </div> 
                    </div>
                    <div class="form-row row-fluid">
                        <div class="span12">
                            <div class="row-fluid">
                                <label class="form-label span3" for="checkboxes">Operadores</label>
                                <div class="span6 controls">   
                                    <select name="operadores" id="operadores">
                                        <option value="=">Igual a</option> 
                                        <option value="<>">Diferente de</option> 
                                        <option value=">">Mayor que</option> 
                                        <option value="<">Menor que</option> 
                                        <option value="=>">Mayor o igual a</option>
                                        <option value="<=">Menor o igual a</option>  
                                    </select>
                                </div> 
                            </div>
                        </div> 
                    </div>
                    <div class="form-row row-fluid">
                        <div class="span12">
                            <div class="row-fluid">
                                <label class="form-label span3" for="confrontar">Confrontar</label>
                                <input class="span6 focused" name="confrontar" id="confrontar" type="text" value="" placeholder="Valor a Confrontar"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-row row-fluid concatenador" style="display:none;">
                        <div class="span12">
                            <div class="row-fluid">
                                <label class="form-label span3" for="checkboxes">Concatenador</label>
                                <div class="span6 controls">   
                                    <select name="contenador" id="contenador">
                                        <option value="AND">AND</option> 
                                        <option value="OR">OR</option> 
                                    </select>
                                </div> 
                            </div>
                        </div> 
                    </div>
                    <div class="form-actions">
                       <button type="button" class="btn btn-info agregar">Agregar</button>
                    </div>
                </form>
            </div>

        </div>
        <div class="content active-filters" style="display:none;">
                
            <form class="horizontal" id="filtrado">
                <input type="hidden" name="id_consulta" value="<?=$id_consulta?>">

                <div class="row-fluid">

                    <div class="span12">
                        <div class="page-header">
                            <h4>Filtros Activos</h4>
                        </div>
                        <table class="responsive table table-condensed">
                            <thead>
                              <tr>
                                <th>Campo</th>
                                <th>Operador</th>
                                <th>Confrontar</th>
                                <th>Concatenador</th>
                                <th>SQL</th>
                                <th>Opciones</th>
                              </tr>
                            </thead>
                            <tbody class="filtros">
                              
                            </tbody>
                        </table>

                    </div><!-- End .span6 -->

                </div>
                <div class="form-actions">
                   <button type="submit" class="btn btn-warning">Aplicar Filtro(s)</button>
                   <button type="button" class="btn btn-danger remover">Remover Filtro(s)</button>
                </div>
            </form>

        </div>

    </div><!-- End .span12 -->

</div><!-- End .row-fluid -->           
<div class="row-fluid">

    <div class="span12">

        <div class="box gradient">

            <div class="title">
                <h4>
                    <span>Vista Previa: <?=$detalles_reporte->que_nombre?></span>
                </h4>
            </div>
            <div class="content noPad clearfix">
                <table cellpadding="0" cellspacing="0" border="0" class="responsive dynamicTable display table table-bordered" width="100%">
                    <thead>
                        <tr>
                            <?=$table_header?>
                        </tr>
                    </thead>
                    <tbody class="resultado">
                        <?php echo $table_tbody?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <?=$table_header?>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <br>
            
            <form class="horizontal" id="pdf" action="<?=base_url('estadistica/crear/reporte')?>" method="POST">
                <input type="hidden" name="id_consulta" value="<?=$id_consulta?>">
                <input type="hidden" name="pdf_filtros" value="">
                <div class="form-actions">
                   <button type="submit" class="btn btn-success" name="crear" value="pdf">Generar PDF</button>
                   <button type="submit" class="btn btn-success" name="crear" value="excel">Generar EXCEL</button>
                </div>
            </form>
        </div><!-- End .box -->

    </div><!-- End .span12 -->

</div><!-- End .row-fluid -->

<script type="text/javascript" src="<?php echo base_url()?>js/reporteria/previa.js"></script>