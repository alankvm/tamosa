<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pdf
{
  protected     $ci;

    public function __construct()
    {
        $this->ci =& get_instance();
    }

    public function cargarPdf( $parametros = null )
    {
        include_once APPPATH . '/third_party/mpdf/mpdf.php';
        if ( $parametros == null ) {
            $parametros = '"utf-8","Letter","","",1,1,1,1,1,1';
        }
        
        return new mPDF( $parametros );
    }

    

}

/* End of file pdf.php */
/* Location: ./application/libraries/pdf.php */
