<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Qmanager extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('reporteria_model');
    }

    /*
        Listado de consultas almacenadas para generacion de reportes
     */
    public function index()
    {
        if (!$this->tank_auth->is_logged_in()) {
            redirect('/auth/login/');
        } else {
            $data['listado_consultas'] = $this->reporteria_model->getListadoConsultas();
            $this->_cargarvista( null, $data );
        }
    }

    /*
        Genera vista previa del reporte generado
     */
    public function previa()
    {
        if (!$this->tank_auth->is_logged_in()) {
            redirect('/auth/login/');
        } else {
            //$this->output->enable_profiler();
            $id_consulta = $this->uri->segment(4);
            $data['table_header']     = $this->reporteria_model->getNombreColumnas( $id_consulta );
            $data['table_tbody']      = $this->reporteria_model->getResultadoConsultaReporte( $id_consulta );
            $data['detalles_reporte'] = $this->reporteria_model->getDetallesReporte( $id_consulta );
            $data['listado_filtros']  = $this->reporteria_model->getListadoFiltrosConsulta( $id_consulta );
            $data['id_consulta']      = $id_consulta;
            $this->_cargarvista( null, $data );
        }
        
    }

    public function filtrar()
    {
        $id_consulta = $this->input->post('consulta');
        $filtro      = $this->input->post('filtro');
        $resultado = $this->reporteria_model->getResultadoConsultaReporte( $id_consulta, $filtro );
        echo $resultado;
    }
    public function _cargarvista( $data = 0, $crud = 0 )
    {   
        $this->load->view('vacia',$crud);   
        if($data!=0)
            $data=array_merge($data,$this->masterpage->getUsuario());
        else
            $data=$this->masterpage->getUsuario();
        $vista=$data['modulo'].'/'.$data['control'].'/'.$data['funcion'];
        $this->masterpage->setMasterPage('masterpage_default');
        $this->masterpage->addContentPage($vista, 'content',$data);
        $this->masterpage->show();
    }

}

/* End of file qmanager.php */
/* Location: ./application/controllers/qmanager.php */