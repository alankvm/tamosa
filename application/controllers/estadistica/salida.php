<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Salida extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('facturas_model');
		$this->load->library('pdf');
	}

	public function index()
	{
		 //Nombre del archivo 
        $filename = 'rpt_'.date('dmY').'_'.substr(uniqid(md5(rand()), true), 0, 7);
        //Guardar PDF creado en proyecto/temp/{nombre}
        $ruta_archivo_pdf = APPPATH."cache/pdf/$filename.pdf";

        //Verificar si el archivo ya existe 
        if ( file_exists( $ruta_archivo_pdf ) == false )
        {
            //Incrementar limite de memoria de proceso
            ini_set( 'memory_limit', '64M' );
            
            //Obtener datos y construir secciones del reporte
            $data['listado'] = $this->facturas_model->cargar_facturas_estado(3);
            $responsables = $this->facturas_model->get_tabla('res_responsable');
			
            
            //Llamar vista y guardar el resultado en HTML
            
            
            //Nueva instancia del creador de PDF
            $pdf = $this->pdf->cargarPdf();
            $pdf->mirrorMargins = 1;
            //Agregar metadata al PDF
            $pdf->SetTitle( 'Reporte Salidas TAMOSA S.A' );
            $pdf->SetAuthor( 'Control de Entradas y Salidas. TAMOSA S.A');
            //Obtener el contenido de print.css
            $stylesheet = file_get_contents( base_url( 'stylesheets/print.css' ) );
            //Importante! el parametro entero 1 indica que esta es una hoja de estilo para el reporte
            $pdf->WriteHTML( $stylesheet, 1 );
            //$pdf->SetHTMLHeader($cabecera);
            //Crear footer del reporte {sistema} - {Numero Pagina} - {fecha y hora}  
            $pdf->SetFooter( $this->config->item('sistema') . '|{PAGENO}|' . date("d/m/Y h:i:s") ); 
            //Escribir el resultado HTML en el PDF
            $total = count($responsables);
            $i=1;
            foreach ($responsables as $key) {                
                $data['responsable'] = $key;                
                $html = $this->load->view('estadistica/rpt_salida', $data, true);
                $pdf->WriteHTML( $html );
                if($i < $total)
                    $pdf->AddPage();
                $i++;
            }
            
            //Guardar el archivo 
            $pdf->Output( $ruta_archivo_pdf, 'D'); 
        }
		
	}

}

/* End of file salida.php */
/* Location: ./application/controllers/salida.php */