<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Facturas extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('bancos_model');
	}

	function index()
	{
		if (!$this->tank_auth->is_logged_in()) {
			redirect('/auth/login/');
		} else {			
			$data['facturas']=$this->bancos_model->cargar_facturas_estado(array(10,11));
			$data['responsables']=$this->bancos_model->cargar_tabla('res_responsable');
			$data['tipo_documento']=$this->bancos_model->get_tabla('tid_tipo_documento', array('tid_tipo'=>2));
			if($_POST) {
				$this->bancos_model->estado_factura($_POST['fac_id'] ,11, $_POST['monto']);				
				redirect('/bancos/facturas/');
			}
			else
			$this->_cargarvista($data);
		}
	}

	function cargar_historial()
	{
		$data['historial']=$this->bancos_model->historial($_POST['id']);
		$this->load->view('facturas/responsable/historial',$data);
	}

	function regresar($id)
	{
		if (!$this->tank_auth->is_logged_in()) {
			redirect('/auth/login/');
		} else {			
			$this->bancos_model->estado_factura($id ,1);
			redirect('/facturas/responsable/');
		}
	}

	function finalizar($id,$valor,$monto)
	{
		if (!$this->tank_auth->is_logged_in()) {
			redirect('/auth/login/');
		} else {
			$diferencia=$valor-$monto;			
			if ($diferencia==0) {
				# factura saldada
				$estado = 7;
			}
			if ($diferencia>0) {
				# factura quedan
				$estado = 8;
			}
			$this->bancos_model->estado_factura($id ,$estado);
			redirect('/facturas/responsable/');
		}
	}

	function _cargarvista($data=0,$crud=0)
	{	
		$this->load->view('vacia',$crud);	
		if($data!=0)
			$data=array_merge($data,$this->masterpage->getUsuario());
		else
			$data=$this->masterpage->getUsuario();
		$vista=$data['modulo'].'/'.$data['control'].'/'.$data['funcion'];
		$this->masterpage->setMasterPage('masterpage_default');
		$this->masterpage->addContentPage($vista, 'content',$data);
		$this->masterpage->show();
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */