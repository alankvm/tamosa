<div class="header">
	<div class="logo">
		<img src="<?=base_url('media/logo/LOGOTAMOSA.png')?>" alt="">
	</div>
	<div class="leyenda">
		<h3>Control de Documentos en Ruta</h3>
	</div>
</div>
<table>
	<thead>
		<tr>			
			<th>Documento</th>
			<th>Cliente</th>
			<th>Valor</th>			
		</tr>
	</thead>
	<tbody>
		<?php $total = 0; ?>
		<?php foreach ( $listado as $factura ) :
		if ($responsable['res_id'] == $factura['res_id']) {
		?>
		<?php $total = $total + $factura['valor'];?>
		<tr>
			<td><?php echo $factura['documento']; ?></td>
			<td><?php echo $factura['cliente']; ?></td>
			<td><?php echo $factura['valor']; ?> </td>			
		</tr>
		<?php }
		endforeach; ?>
	</tbody>
	
</table>
<p class="total">Total: $<?php echo number_format($total, 2); ?></p>
<h2>Responsable: <?php echo $responsable['res_nombre'].' '.$responsable['res_apellido']?></h2>

<table>
	<thead>
		<tr>
			<th>Salida</th>
			<th>Entrada</th>
			<th>Salida</th>
			<th>Entrada</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td rowspan="2"><img src="<?php echo base_url()?>media/reporte/combustible.png"></td>
			<td rowspan="2"><img src="<?php echo base_url()?>media/reporte/combustible.png"></td>
			<td>Hora de salida</td>
			<td>Hora de llegada</td>
		</tr>
		<tr>
			<td>Kilometraje inicial</td>
			<td>Kilometraje final</td>
		</tr>
	</tbody>
</table>
