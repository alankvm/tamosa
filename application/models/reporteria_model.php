<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reporteria_Model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        //Inicializar variables
        $this->db_driver = $this->db->platform();
        $this->db_name = $this->db->database;
        $this->load->library('dbmetadata');
    }

    /**
     * Obtiene listado de tablas en la base de datos
     * @return object listado de tablas pertenecientes a la base de datos seleccionada
     */
    public function getListadoTablas()
    {
        $resultado = $this->dbmetadata->getTablas();
        return $resultado;
    }

    /**
     * Inserta datos necesarios para generar un nuevo reporte 
     * @return boolean resultado de la operacion
     */
    public function nuevoReporte()
    {
        //Datos del reporte
        $data_query = array(
            'que_nombre'      => $this->input->post('name'),
            'que_descripcion' => $this->input->post('description'),
            'que_consulta'    => $this->input->post('query')
            );

        $resultado = $this->db->insert( 'que_query', $data_query );
        $id_query  = $this->db->insert_id();
        //Filtros del reporte
        $filtros   = $this->input->post('filtros');
        
        //Si hay filtros, insertarlos
        if ( count( $filtros ) > 0 ) {
            foreach ($filtros as $filtro) {
                //Extraer el campo y alias campo[0]|alias[1]
                $elementos = explode("|", $filtro);

                $data_filtro = array(
                    'fil_id_que' => $id_query,
                    'fil_campo' => $elementos[0],
                    'fil_alias' => $elementos[1]
                    );

                $resultado = $this->db->insert( 'fil_filtro', $data_filtro );
            }
        }

        return $resultado;
    }

    /**
     * Obtiene nombres de columna del reporte seleccionado y genera estructura 
     * HTML para las cabeceras de los reportes
     * @param  integer $id_consulta ID consulta
     * @return string estructura HTML de las cabeceras, para el reporte
     */
    public function getNombreColumnas( $id_consulta = 0 )
    {
        //Obtener detalles del consulta
        $consulta   =  $this->getConsulta( $id_consulta )->row()->que_consulta;

        //Sacar los alias y ponerlos como table header                        
        preg_match_all('/AS (\w)*/', $consulta, $matches);
        
        //Inicializar variable
        $table_header = "<th>#</th>";

        //construir table header
        foreach ( $matches[0] as $header ) {
            //Eliminar espacios y remover el identificado 'AS'
            $cabecera = trim( substr( $header, 2 ) );
            //Construir el Table Header 
            $table_header .= "<th>$cabecera</th>";
        }
        
        return ( $table_header != "" )
        ? $table_header
        : false;
    }

    /**
     * Obtiene nombre de columnas del reporte
     * @param  integer $id_consulta ID de consulta
     * @return array nombres de columnas
     */
    public function getNombreColumnasSQL( $id_consulta = 0 )
    {
        $consulta   =  $this->getConsulta( $id_consulta )->row()->que_consulta;

        //Sacar los alias y ponerlos como table header                        
        preg_match_all('/AS (\w)*/', $consulta, $matches);
        $nomrbres = array();

        //construir table header
        foreach ( $matches[0] as $header ) {
            $nombres[] = trim(substr($header, 2));
        }

        return ( count( $nombres ) > 0 )
        ? $nombres
        : false;
    }

    /**
     * Obtiene resultados de la consulta del reporte y construye 
     * la estructura HTML para incluir en el TBODY (listado de TR a incluir)
     * @param  integer $id_consulta ID de consulta
     * @param  string $filtro SQL con filtrado
     * @return string estructura de HTML para incluir en el TBODY
     */
    public function getResultadoConsultaReporte( $id_consulta = 0, $filtro = "" )
    {
        //Obtiene datos del reporete seleccionados
        $consulta   =   $this->getConsulta( $id_consulta )->row()->que_consulta;
        //Agregar Filtros si no los tiene
        if ( $filtro != "" ) {
            $consulta .= " " . $filtro;
        }

        //Obtener listado de nombres
        $nombres    =   $this->getNombreColumnasSQL( $id_consulta );

        //Obtiene resultados de la ejecucion de la consulta
        $resultado  =   $this->db->query( $consulta );

        //Inicializando filas
        $rows = "";

        //Verificar si el resultado de la consulta tiene registros 
        if ( is_object( $resultado ) && $resultado->num_rows() > 0 ) {
            //Inicializando TR para incluir en TBODY
           
            //Inicializando contador de filas
            $numero_fila = 1;
            foreach ( $resultado->result() as $fila ) {
                $rows .= "<tr class='odd gradeX'>";
                //Agregar TD de numero de fila
                $rows .= "<td>";
                $rows .= $numero_fila++;
                $rows .= "</td>";

                //Obtener resultados por cada nombre de columna
                foreach ( $nombres as $entidad ) {
                    //Agregar los TD 
                    $rows .= "<td>";
                    $rows .= ( $fila->$entidad == null ) ? 'No Definido' : $fila->$entidad;
                    $rows .= "</td>";
                }
                //Terminar fila
                $rows .= "</tr>";
            }

        } else {
            $rows .= "<td> 0 Registros Encontrados</td>";
            foreach ( $nombres as $entidad ) {
                $rows .= "<td></td>";
            }
        }
      
        return ( $rows )
        ? trim($rows)
        : false;
    }

    /**
     * Obtiene listado de consultas almacenadas en la base de datos para realizar reportes
     * @return object listado de consultas para reporteria
     */
    public function getListadoConsultas()
    {
        $resultado = $this->db->get('que_query');
        
        return ( is_object( $resultado ) && $resultado->num_rows() > 0 )
        ? $resultado
        : false;
    }

    /**
     * Obtiene Detalles del Reporte Seleccionado
     * @param  integer $id_consulta ID de reporte almacenado
     * @return object detalles de reporte seleccionado
     */
    public function getDetallesReporte( $id_consulta = 0 )
    {
        $resultado = $this->getConsulta( $id_consulta );

        return ( is_object( $resultado ) && $resultado->num_rows() > 0 )
        ? $resultado->row()
        : false;
    }

    /**
     * Obtiene consulta por ID de consulta almacenada
     * @param  integer $id_consulta ID de consulta
     * @return object resultado de la consulta
     */
    public function getConsulta( $id_consulta = 0 )
    {
        return $this->db->get_where( 'que_query', array( 'que_id' => $id_consulta ) );
    }

    /**
     * Obtiene listado de filtros de consulta
     * @param  integer $id_consulta ID de consulta
     * @return object listado de filtros seleccionados
     */
    public function getListadoFiltrosConsulta( $id_consulta = 0 )
    {
        $resultado = $this->db->get_where( 'fil_filtro', array( 'fil_id_que' => $id_consulta ) );

        return ( is_object( $resultado ) && $resultado->num_rows() > 0 )
        ? $resultado
        : false;
    }

}

/* End of file reporteria_model.php */
/* Location: ./application/models/reporteria_model.php */