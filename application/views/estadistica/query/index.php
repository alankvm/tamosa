
<div class="row-fluid principal">

    <div class="span12">

        <div class="box">
            <!-- FeedBack from reporte -->
            <?php if ( isset( $_GET['resultado'] ) && $_GET['resultado'] == 1 ): ?>                    
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>Buen Trabajo!</strong> La consulta para el reporte ha sido generada exitosamente.
            </div>
            <?php elseif ( isset( $_GET['resultado'] ) && $_GET['resultado'] == 0 ): ?>
            <div class="alert alert-error">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>oh-oh!</strong> Ocurri&oacute; un error, por favor intentalo de nuevo.
            </div>
            <?php endif; ?>
            <div class="title">

                <h4>
                    <span>Nuevo Reporte</span>
                </h4>
                
            </div>

            <div class="content">
                <form class="horizontal" id="nuevorpt">

                    <div class="form-row row-fluid">
                        <div class="span12">
                            <div class="form-row row-fluid lugar-consulta">
                                <div class="span12">
                                    <div class="row-fluid">
                                        <label class="form-label span3" for="lugar">Nombre del Reporte</label>
                                        <input class="span8" name="nombre" id="nombre" type="text" value="" placeholder="Nombre del Reporte"/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row row-fluid lugar-consulta">
                                <div class="span12">
                                    <div class="row-fluid">
                                        <label class="form-label span3" for="lugar">Descripci&oacute;n</label>
                                        <input class="span8" name="descripcion" id="descripcion" type="text" value="" placeholder="&iquest;C&uacute;al es el objetivo del reporte?"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row-fluid">
                                <label class="form-label span3" for="checkboxes">Listado Tablas</label>
                                <div class="span8 controls">   
                                    <select name="tablas" id="tablas" class="nostyle" style="width:100%;" placeholder="Tabla Principal del Reporte">
                                        <option></option>
                                <?php   if ( is_object( $listado_tablas ) ): 
                                            foreach ( $listado_tablas->result() as $tabla ): ?>
                                        <option value="<?=$tabla->nombre_tabla?>"><?=$tabla->nombre_tabla?></option>
                                <?php       endforeach;
                                        else: ?>
                                        <option value="-1">Cero Registros Encontrados</option>
                                <?php   endif; ?>
                                    </select>
                                </div> 
                            </div>
                        </div> 
                    </div>
                   
                </form>

            </div>

        </div><!-- End .box -->

    </div><!-- End .span6 -->

</div><!-- End .row-fluid --> 
<div class="generador">
<div class="row-fluid">

    <div class="span12" id="query">

        <div class="box">

            <div class="title">

                <h4>
                    <span class="icon16 icomoon-icon-code"></span>
                    <span>Vista Previa de la Consulta SQL</span>
                </h4>
                <a href="#" class="minimize">Minimize</a>
            </div>
            
            <div class="content">
            <!-- IMPORTANT! no dejar espacios, pre reconoce todos los espacios en codigo -->
<pre class="prettyprint linenums query"><span class="dmlselect red">SELECT</span> <span class="display-fields">*</span> 
<span class="dmlfrom red">FROM</span> <span class="selected-table">tabla</span>
<span class="joins"></span>
           </div>

        </div><!-- End .box -->

    </div><!-- End .span6 -->

    <div class="span5" id="listado-filtros">

        <div class="box">

            <div class="title">

                <h4>
                    <span class="icon16 icomoon-icon-equalizer-2"></span>
                    <span>Listado de Filtros</span>
                </h4>
                <a href="#" class="minimize">Minimize</a>
            </div>
            <div class="content noPad">
                <table class="table">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Campo</th>
                      <th>Alias</th>
                    </tr>
                  </thead>
                  <tbody class="filtros">
                    <!-- cargar listado de filtros -->
                  </tbody>
                </table>
            </div>

        </div><!-- End .box -->

    </div><!-- End .span6 -->

</div><!-- End .row-fluid -->

<div class="row-fluid fields">

    <div class="span12">

        <div class="box">

            <div class="title">

                <h4>
                    <span>Listado de Campos</span>
                </h4>
                
            </div>

            <div class="content">
                <form class="horizontal" id="frmagregar">

                    <div class="form-row row-fluid">
                        <div class="span12">
                            <div class="row-fluid">
                                <label class="form-label span3" for="checkboxes">Mostrar</label>
                                <div class="span8 controls">   
                                    <select name="campos" id="campos" class="nostyle" style="width:100%;" placeholder="Seleccione un campo">
                                        <option></option>
                                        <!-- Listado de Campos - tabla seleccionada -->
                                    </select>
                                </div> 
                            </div>
                        </div> 
                    </div>
                    <div class="form-row row-fluid lugar-consulta">
                        <div class="span12">
                            <div class="row-fluid">
                                <label class="form-label span3" for="lugar">Ver como</label>
                                <input class="span8" name="alias" id="alias" type="text"  value="" placeholder="Alias del campo seleccionado"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-row row-fluid">
                        <div class="span12">
                            <div class="row-fluid">

                                <label class="form-label span3" for="checkboxes">&iquest;Usar como filtro?</label>
                                
                                <div class="span8 controls">
                                    
                                    <div class="left marginR10">
                                        <input type="checkbox" id="filtro" class="ibuttonCheck nostyle" /> 
                                    </div>
                                
                                </div>
                                
                            </div>
                        </div> 
                    </div>
                    <div class="form-actions">
                        <button type="button" class="btn btn-info agregar-field">Agregar</button>
                        <button type="button" class="btn btn-warning relations">Mostrar Relaciones</button>
                        <button type="button" class="btn btn-primary finalizar">Finalizar</button>
                    </div>
                   
                </form>

            </div>

        </div><!-- End .box -->

    </div><!-- End .span6 -->

</div><!-- End .row-fluid --> 

<div class="row-fluid relaciones">

    <div class="span12">

        <div class="box">

            <div class="title">

                <h4>
                    <span>Tablas Relacionadas</span>
                </h4>
                
            </div>

            <div class="content">
                <form class="horizontal" id="frmforeign">

                    <div class="form-row row-fluid">
                        <div class="span12">
                            <div class="row-fluid">
                                <label class="form-label span3" for="checkboxes">Relaci&oacute;n</label>
                                <div class="span8 controls">   
                                    <select name="foraneas" id="foraneas" class="nostyle" style="width:100%;" placeholder="Seleccione un relaci&oacute;n">
                                        <option></option>
                                        <!-- Listado de Campos - tabla seleccionada -->
                                    </select>
                                </div> 
                            </div>
                        </div> 
                    </div>
                     <div class="form-row row-fluid">
                        <div class="span12">
                            <div class="row-fluid">
                                <label class="form-label span3" for="checkboxes">Campos Foraneos</label>
                                <div class="span8 controls">   
                                    <select name="foreignkeys" id="foreignkeys" class="nostyle" style="width:100%;" placeholder="Seleccione un Campo Foraneo">
                                        <option></option>
                                        <!-- Listado de Campos - tabla seleccionada -->
                                    </select>
                                </div> 
                            </div>
                        </div> 
                    </div>
                    <div class="form-row row-fluid lugar-consulta">
                        <div class="span12">
                            <div class="row-fluid">
                                <label class="form-label span3" for="lugar">Ver como</label>
                                <input class="span8" name="foreign_alias" id="foreign_alias" type="text" value="" placeholder="Alias del campo seleccionado"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-row row-fluid">
                        <div class="span12">
                            <div class="row-fluid">

                                <label class="form-label span3" for="checkboxes">&iquest;Usar como filtro?</label>
                                
                                <div class="span8 controls">
                                    
                                    <div class="left marginR10">
                                        <input type="checkbox" id="foreign_filtro" class="ibuttonCheck nostyle" /> 
                                    </div>
                                
                                </div>
                                
                            </div>
                        </div> 
                    </div>
                    <div class="form-actions">
                        <button type="button" class="btn btn-info agregar-relacion">Agregar</button>
                        <button type="button" class="btn btn-inverse regresar">Regresar</button>
                        <button type="button" class="btn btn-primary finalizar">Finalizar</button>
                    </div>
                   
                </form>

            </div>

        </div><!-- End .box -->

    </div><!-- End .span6 -->

</div><!-- End .row-fluid --> 

</div>
<div class="row-fluid finalizar">

    <div class="span12">

        <div class="box">

            <div class="title">

                <h4>
                    <span>Resumen</span>
                </h4>
                
            </div>

            <div class="content">
                <dl>
                    <dt>Nombre del Reporte</dt>
                    <dd id="rptname"></dd>
                    <dt>Descripci&oacute;n del Reporte</dt>
                    <dd id="rptdesc"></dd>
                    <dt>&iquest;Deseas guardar este reporte?</dt>
                </dl>
                <form class="horizontal" method="POST" action="<?=base_url('estadistica/query/nuevo')?>" id="generarQuery">

                    <input type="hidden" name="name" id="hiddenName" value="">
                    <input type="hidden" name="description" id="hiddenDescription" value="">
                    <input type="hidden" name="query" id="hiddenQuery" value="">
                    <div class="inputfilters">
                        
                    </div>
                    <div class="form-actions">
                        <button type="submit" class="btn btn-success"><span class="icon16 icomoon-icon-checkmark white"></span>Si</button>
                        <a href="<?=base_url("estadistica/query")?>" class="btn btn-danger"><span class="icon16 icomoon-icon-cancel-2 white"></span>No</a>
                    </div>
                   
                </form>

            </div>

        </div><!-- End .box -->

    </div><!-- End .span6 -->

</div><!-- End .row-fluid --> 



<script src="<?php echo base_url()?>js/reporteria/query.js"></script>
<script src="<?php echo base_url()?>js/reporteria/validacion_query.js"></script>