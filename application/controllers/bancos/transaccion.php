<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Transaccion extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('bancos_model');
		$this->load->model('facturas_model');
		$this->load->library('pdf');
	}

       public function new_crud(){
        $db_driver = $this->db->platform();
        $model_name = 'grocery_crud_model_'.$db_driver;
        $model_alias = 'm'.substr(md5(rand()), 0, rand(4,15) );

        unset($this->{$model_name});
        $this->load->library('grocery_CRUD');
        $crud = new Grocery_CRUD();
        if (file_exists(APPPATH.'/models/'.$model_name.'.php')){
            //$this->load->model('grocery_crud_model'); --Ya esta cargada la libreria en autoload :P
            $this->load->model('grocery_crud_generic_model');
            $this->load->model($model_name,$model_alias);
            $crud->basic_model = $this->{$model_alias};
        }
        return $crud;
    }

	function index()
	{
		$data['cuentas']=$this->bancos_model->get_cuentas();		
		$this->_cargarvista($data);
	}

	function entradas($id)
	{
		$data['transacciones']=$this->bancos_model->get_entrada($id);
		$data['cuenta'] = $this->bancos_model->get_cuenta($id);	
		$data['id'] = $id;
		$this->_cargarvista($data);
	}

		function salidas($id)
	{
		$data['transacciones']=$this->bancos_model->get_salida($id);
		$data['cuenta'] = $this->bancos_model->get_cuenta($id);	
		$data['id'] = $id;
		$this->_cargarvista($data);
	}

	function agregar_entrada($id)
	{
		$data['facturas']=$this->bancos_model->cargar_facturas_estado(array(11));
		$data['cuenta'] = $this->bancos_model->get_cuenta($id);	
		$data['id'] = $id;
		if ($_POST) {			
			$transaccion = array(
				'tra_id_cue' => $id,
				'tra_entrada' => $_POST['entrada'],
				'tra_comprobante' => $_POST['comprobante'],
				'tra_concepto' => $_POST['concepto'],
				'tra_id_usu' => $this->tank_auth->get_user_id(),
				'tra_fecha' => date('Ymd') );
			$tra_id = $this->bancos_model->add_registro('tra_transaccion', $transaccion);

			foreach ($_POST['factura'] as $key) {
				$fact = array(
					'txf_id_fac' => $key,
					'txf_id_tra' => $tra_id );
				$this->bancos_model->add_registro('txf_transaccionxfactura', $fact);
				$this->bancos_model->estado_factura($key ,12);
			}
			redirect('bancos/transaccion/entradas/'.$id);
		}
		$this->_cargarvista($data);
	}

	function agregar_salida($id)
	{
		$data['facturas']=$this->bancos_model->cargar_vendedores();
		$data['cuenta'] = $this->bancos_model->get_cuenta($id);	
		$data['id'] = $id;
		if ($_POST) {			
			$transaccion = array(
				'tra_id_cue' => $id,
				'tra_salida' => $_POST['entrada'],
				'tra_comprobante' => $_POST['comprobante'],
				'tra_concepto' => $_POST['concepto'],
				'tra_id_usu' => $this->tank_auth->get_user_id(),
				'tra_fecha' => date('Ymd') );
			$tra_id = $this->bancos_model->add_registro('tra_transaccion', $transaccion);

			foreach ($_POST['factura'] as $key) {
				$fact = array(
					'txv_id_ven' => $key,
					'txv_id_tra' => $tra_id );

				$this->bancos_model->add_registro('txv_transaccionxvendedor', $fact);
				$facturas =  $this->bancos_model->cargar_facturas_vendedor($key);				
				foreach ($facturas as $key2) {
					$this->bancos_model->estado_factura($key2['fac_id'] ,13);
				}				
			}
			redirect('bancos/transaccion/salidas/'.$id);
		}
		$this->_cargarvista($data);
	}

	function estado($id)
	{
		$data['transacciones']=$this->bancos_model->get_estado($id);
		$data['cuenta'] = $this->bancos_model->get_cuenta($id);	
		$data['id'] = $id;
		$this->_cargarvista($data);
	}

	function cargar_historial()
	{
		$data['historial']=$this->bancos_model->historial($_POST['id']);		
		$this->load->view('facturas/responsable/historial',$data);
	}

	function regresar($id)
	{
		if (!$this->tank_auth->is_logged_in()) {
			redirect('/auth/login/');
		} else {			
			$this->bancos_model->del_registro('des_destino',array('des_id_fac'=>$id));
			$this->bancos_model->estado_factura($id ,6);
			redirect('/facturas/responsable/');
		}
	}

	function _cargarvista($data=0,$crud=0)
	{	
		$this->load->view('vacia',$crud);	
		if($data!=0)
			$data=array_merge($data,$this->masterpage->getUsuario());
		else
			$data=$this->masterpage->getUsuario();
		$vista=$data['modulo'].'/'.$data['control'].'/'.$data['funcion'];
		$this->masterpage->setMasterPage('masterpage_default');
		$this->masterpage->addContentPage($vista, 'content',$data);
		$this->masterpage->show();
	}

	public function pdfRemesas($id)
	{
		 //Nombre del archivo 
        $filename = 'rpt_'.date('dmY').'_'.substr(uniqid(md5(rand()), true), 0, 7);
        //Guardar PDF creado en proyecto/temp/{nombre}
        $ruta_archivo_pdf = APPPATH."cache/pdf/$filename.pdf";

        //Verificar si el archivo ya existe 
        if ( file_exists( $ruta_archivo_pdf ) == false )
        {
            //Incrementar limite de memoria de proceso
            ini_set( 'memory_limit', '64M' );
            
            //Obtener datos y construir secciones del reporte
            $data['listado'] = $this->facturas_model->cargar_facturas_estado(3);
            $responsables = $this->facturas_model->get_tabla('res_responsable');
			
            
            //Llamar vista y guardar el resultado en HTML
            
            
            //Nueva instancia del creador de PDF
            $pdf = $this->pdf->cargarPdf();
            $pdf->mirrorMargins = 1;
            //Agregar metadata al PDF
            $pdf->SetTitle( 'Reporte Salidas TAMOSA S.A' );
            $pdf->SetAuthor( 'Control de Entradas y Salidas. TAMOSA S.A');
            //Obtener el contenido de print.css
            $stylesheet = file_get_contents( base_url( 'stylesheets/print.css' ) );
            //Importante! el parametro entero 1 indica que esta es una hoja de estilo para el reporte
            $pdf->WriteHTML( $stylesheet, 1 );
            //$pdf->SetHTMLHeader($cabecera);
            //Crear footer del reporte {sistema} - {Numero Pagina} - {fecha y hora}  
            $pdf->SetFooter( $this->config->item('sistema') . '|{PAGENO}|' . date("d/m/Y h:i:s") ); 
            //Escribir el resultado HTML en el PDF
            $total = count($responsables);
            $i=1;
            foreach ($responsables as $key) {
                $data['responsable'] = $key;
                $html = $this->load->view('estadistica/rpt_salida', $data, true);
                $pdf->WriteHTML( $html );
                if($i < $total)
                    $pdf->AddPage();
                $i++;
            }
            
            //Guardar el archivo 
            $pdf->Output( $ruta_archivo_pdf, 'D'); 
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */