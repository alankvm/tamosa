                    <div class="row-fluid">
		<div class="span8">			
			<a class="btn btn-large" href="<?php echo base_url().'bancos/transaccion/agregar_entrada/'.$id?>" title="Agregar Transaccion"><strong>Agregar Transaccion</strong></a>			
		</div>
		<div class='clear'></div>
	</div>
	<div class="row-fluid">

                        <div class="span12">

                            <div class="box gradient">

                                <div class="title">
                                    <h4>
                                        <span>Banco: <?php echo $cuenta['ban_nombre']?>   -   Cuenta: <?php echo $cuenta['cue_numero']?></span>
                                    </h4>
                                </div>
                                 <div class="content noPad clearfix">
<table cellpadding="0" cellspacing="0" border="0" class="responsive dynamicTable display table table-bordered" width="100%">
	<thead>
		<tr>
			<th>ID</th>
			<th>Concepto</th>
			<th>Comprobante</th>
			<th>Monto</th>
			<th>Saldo</th>
			<th>Fecha</th>
			<th>Acciones</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>1</td>
			<td> Saldo inicial de cuenta</td>
			<td> - </td>
			<td>(+)<?php echo $cuenta['cue_saldo_inicial']?></td>
			<td><?php echo $cuenta['cue_saldo_inicial']?></td>
			<td><?php echo date('d-m-Y', strtotime($cuenta['cue_creacion']))?></td>
			<td></td>
		</tr>
		<?php 
		$saldo = $cuenta['cue_saldo_inicial'];
		foreach ($transacciones as $key) { 
			$saldo = $saldo + $key['tra_entrada'] -$key['tra_salida'];
			?>
		<tr>
			<td><?php echo $key['tra_id']+1?></td>
			<td><?php echo $key['tra_concepto']?></td>
			<td><?php echo $key['tra_comprobante']?></td>
			<td><?php echo ($key['tra_entrada']>0) ? "(+)".$key['tra_entrada']:"(-)".$key['tra_salida'];?></td>
			<td><?php echo $saldo?></td>
			<td><?php echo date('d-m-Y', strtotime($key['tra_fecha']))?></td>
			<td align="left" style="width:20%">
				<div class='tools'>
					<div class="btn-group">
						<button class="btn">Acciones</button>
						<button class="btn dropdown-toggle" data-toggle="dropdown">
							<span class="caret"></span>
						</button>
						<ul class="dropdown-menu">
							<li>
								<a href="#" class="acciones crud-action" title="accion"><span class="icon16 "></span>Ver</a>
							</li>
						</ul>
					</div>
				</td>
			</tr>
			<?php } ?>
		</tbody>

	</table>
		 </div>
	</div><!-- End .box -->

                        </div><!-- End .span12 -->

                    </div><!-- End .row-fluid -->