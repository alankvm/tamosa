<div class="row-fluid">

	<div class="span12">

		<div class="box gradient">

			<div class="title">
				<h4>
					<span>Cuentas bancarias</span>
				</h4>
			</div>
			<div class="content noPad clearfix">
				<table cellpadding="0" cellspacing="0" border="0" class="responsive dynamicTable display table table-bordered" width="100%">
					<thead>
						<tr>
							<th>Banco</th>
							<th>Numero de cuenta</th>
							<th>Saldo</th>
							<th>Acciones</th>
						</tr>
					</thead>
					<tbody>
						<?php
						foreach ($cuentas as $key) { ?>
						<tr>
							<td><?php echo $key['ban_nombre']?></td>
							<td><?php echo $key['cue_numero']?></td>
							<td><?php echo $key['cue_saldo_inicial'] + $key['entrada'] - $key['salida']?></td>							
							<td align="left" style="width:20%">
								<div class='tools'>
									<div class="btn-group">
										<button class="btn">Transacciones</button>
										<button class="btn dropdown-toggle" data-toggle="dropdown">
											<span class="caret"></span>
										</button>
										<ul class="dropdown-menu">
											<li>
												<a href="<?php echo base_url()?>bancos/transaccion/entradas/<?php echo $key['cue_id']?>" class="acciones crud-action" title="accion"><span class="icon16 silk-icon-enter"></span>Control de Entradas</a>
											</li>
											<li>
												<a href="<?php echo base_url()?>bancos/transaccion/salidas/<?php echo $key['cue_id']?>" class="acciones crud-action" title="accion"><span class="icon16 silk-icon-exit"></span>Control de Salidas</a>
											</li>
											<li>
												<a href="<?php echo base_url()?>bancos/transaccion/estado/<?php echo $key['cue_id']?>" class="acciones crud-action" title="accion"><span class="icon16 icomoon-icon-coins"></span>Estado de cuenta</a>
											</li>
											<li>
												<a href="<?php echo base_url()?>bancos/transaccion/pdfRemesas/<?php echo $key['cue_id']?>" class="acciones crud-action" title="accion"><span class="icon16 icomoon-icon-coins"></span>Reporte de remesas</a>
											</li>
											<li>
												<a href="<?php echo base_url()?>bancos/transaccion/pdfEstado/<?php echo $key['cue_id']?>" class="acciones crud-action" title="accion"><span class="icon16 icomoon-icon-coins"></span>Reporte de estado de cuenta</a>
											</li>
										</ul>
									</div>
								</td>
							</tr>
							<?php } ?>
						</tbody>

					</table>
				</div>
			</div><!-- End .box -->

		</div><!-- End .span12 -->

                    </div><!-- End .row-fluid -->