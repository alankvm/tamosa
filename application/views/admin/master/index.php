<div class="row-fluid">

    <div class="span12">
        <div class="page-header">
            <h4>Detalle De Remesa Correspondiente A: <?=$detalle_remesa?></h4>
        </div>
        <table class="responsive table table-bordered">
            <thead>
              <tr>
                <th>#</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Username</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>1</td>
                <td>Mark</td>
                <td>Otto</td>
                <td>@mdo</td>
              </tr>
              <tr>
                <td>2</td>
                <td>Jacob</td>
                <td>Thornton</td>
                <td>@fat</td>
              </tr>
              <tr>
                <td>3</td>
                <td>Larry</td>
                <td>the Bird</td>
                <td>@twitter</td>
              </tr>
            </tbody>
        </table>

    </div><!-- End .span12 -->

</div><!-- End .container-fluid -->