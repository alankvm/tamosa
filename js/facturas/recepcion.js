var recepcion = {
    fn: {
        asignarTipoRecibo: function () {
            var elementos = $("input[name='pedidos[]']"),
                inputHidden = "",
                formElementos = $("#seleccionados");

            $.each(elementos, function (index, element) {
                if ($(element).is(":checked")) {
                    inputHidden += '<input type="hidden" name="facturas[]" value="' + $(element).val() + '">';
                }
            });
             formElementos.append(inputHidden);
            //return false;
        }
    }
};

jQuery(document).ready(function($) {
    $("#frm-recepcion").on("submit", recepcion.fn.asignarTipoRecibo);
});