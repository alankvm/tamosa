<div class="row-fluid">

    <div class="span12">
<a href="<?=base_url('facturas/responsable')?>" class="btn btn-danger" style="position: relative; z-index: 1000; float: right; margin-right: 30%;">Volver</a>
<a href="<?=base_url('estadistica/salida')?>" class="btn btn-primary" style="position: absolute; right: 2%; z-index: 1000; top: 17%">Generar Reporte de Salidas</a>
    </div>
</div>
<br>

<div class="row-fluid">

    <div class="span12">

            <div class="content noPad clearfix">
                <table class="responsive dynamicTable display table table-bordered">
                   <thead>
                      <tr>
                        <th>
                            Documento
                        </th>   
                        <th>
                            Cliente
                        </th>           
                        <th>
                            Responsable
                        </th>
                        <th>
                            Salida
                        </th>
                        <th>
                            Acciones
                        </th>
                    </tr>
                </thead>        
                <tbody style="font-size: 11px;">     
                  <?php foreach ($facturas as $key) { ?>
                  <tr>
                    <td>
                        <?php echo $key['documento']?>
                    </td>
                    <td style="text-align: left;">
                        <?php echo $key['cliente']?>
                    </td>           
                    <td>
                        <?php echo $key['res_nombre'].' '.$key['res_apellido']?>
                    </td>
                    <td>
                        <?php echo $key['tid_salida']?>
                    </td>
                    
                        <td>
                <input type="checkbox" class="tip" value="<?php echo $key['fac_id'] ?>" name="pedidos[]" title="Asignar Tipo de Salida">
                <a href="#" data-id="<?=$key['fac_id']?>" name="regresar[]"><span aria-hidden="true" class="icomoon-icon-cancel tip" title="Regresar a Asignacion"></span>&nbsp;</a> 
                <a data-toggle="modal" href="#historial" data-id="<?php echo $key['fac_id'] ?>" onclick="cargar_historial(<?php echo $key['fac_id'] ?>)" title="Ver historial"><span aria-hidden="true" class="minia-icon-calendar-2"></span></a> |                 
            </td>   

                </tr>
                <?php } ?>
            </tbody>
        </table>

    </div>



</div><!-- End .span12 -->

</div><!-- End .row-fluid -->
<div class="row"></div>
<div class="row-fluid">
    <form class="form" method="POST" id="frm-salida">
        <div id="seleccionados">
            
        </div>
        <select name="tipo_salida_multiple" id="tipo_salida_multiple" class="nostyle" style="width:50%; position: relative; float: right; bottom: -20px; margin-right: 20%;">
            <?php foreach ($tipo_documento as $key) {?>
                <option value="<?php echo $key['tid_id']?>"><?php echo $key['tid_nombre']?></option>
            <?php } ?>
        </select>
        <div class="form-actions">
            <button class="btn btn-primary" type="submit">Asignar Tipo de Salida</button>
        </div>
<!-- Remove el return false de salida.js -->
    </form>
</div>
<div class="modal fade hide" id="asignar">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h3>Asignar monto</h3>
    </div>
    <div class="modal-body">
        <div class="box">            
            <div class="content">               
                <form class="form-horizontal" method="POST">
                    <div class="form-row row-fluid">
                        <div class="span12">
                            <div class="row-fluid">
                                <label class="form-label span4" for="nombres">Tipo de salida</label>
                                <div class="span8 controls">
                                    <select name="tipo_salida" id="tipo_salida">
                                        <?php foreach ($tipo_documento as $key) { ?>
                                            <option value="<?php echo $key["tid_id"]?>"><?php echo $key["tid_nombre"]?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-row row-fluid">
                        <div class="span12">
                            <div class="row-fluid">
                                <label class="form-label span4" for="nombres">Observaciones</label>
                                <textarea class="span8 text" id="obs" type="text" name="obs"></textarea>
                            </div>
                        </div>
                    </div>                  
                    <div class="form-actions">
                        <input type="hidden" id="fac_id" name="fac_id">
                        <input type="hidden" id="monto" name="monto">
                       <button type="submit" class="btn btn-info">Guardar</button>
                       <button type="button" class="btn">Cancelar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal">Cerrar</a>
    </div>
</div>

<div class="modal fade hide" id="historial">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h3>Historial</h3>
    </div>
    <div class="modal-body">
        <div class="box">
            <div class="content">               
                <div id="tabla_historial"></div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal">Cerrar</a>
    </div>
</div>
<script src="<?php echo base_url("js/facturas/salidas.js"); ?>"></script>