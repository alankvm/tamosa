<div class="row-fluid">

    <div class="span12">

        <div class="box gradient">

            <div class="title">
                <h4>
                    <span>Reportes Almacenados</span>
                </h4>
            </div>
            <div class="content noPad clearfix">
                <table cellpadding="0" cellspacing="0" border="0" class="responsive dynamicTable display table table-bordered" width="100%">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nombre</th>
                            <th>Descripc&oacute;n</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                <?php   if ( $listado_consultas ):
                            $numero_fila = 1;
                            foreach ( $listado_consultas->result() as $reporte ): ?>
                        <tr class="odd gradeX">
                            <td><?=$numero_fila++?></td>
                            <td><?=$reporte->que_nombre?></td>
                            <td><?=$reporte->que_descripcion?></td>
                            <td>
                                <div class="controls center">
                                    <a href="<?=base_url('estadistica/qmanager/previa/' . $reporte->que_id)?>" class="tip" oldtitle="Ver Reporte" title="Ver Reporte" aria-describedby="ui-tooltip-7">
                                        <span class="wpzoom-clipboard"></span>
                                    </a>
                                    <!-- <a href="#" class="tip" oldtitle="Remove task" title=""><span class="icon12 icomoon-icon-remove"></span></a> -->
                                </div>
                            </td>
                        </tr>
                <?php       endforeach;
                        else: ?>
                        <tr class="odd gradeX">
                            <td></td>
                            <td>No se Encontro Ningun Registro</td>
                            <td></td>
                            <td></td>
                        </tr>
                <?php endif; ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>#</th>
                            <th>Nombre</th>
                            <th>Descripc&oacute;n</th>
                            <th>Acciones</th>
                        </tr>
                    </tfoot>
                </table>
            </div>

        </div><!-- End .box -->

    </div><!-- End .span12 -->

</div><!-- End .row-fluid -->