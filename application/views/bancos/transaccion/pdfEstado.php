<table>
	<thead>
		<tr>
			<th>ID</th>
			<th>Concepto</th>
			<th>Comprobante</th>
			<th>Monto</th>
			<th>Saldo</th>
			<th>Fecha</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>1</td>
			<td> Saldo inicial de cuenta</td>
			<td> - </td>
			<td>(+)<?php echo $cuenta['cue_saldo_inicial']?></td>
			<td><?php echo $cuenta['cue_saldo_inicial']?></td>
			<td><?php echo date('d-m-Y', strtotime($cuenta['cue_creacion']))?></td>
		</tr>
		<?php 
		$saldo = $cuenta['cue_saldo_inicial'];
		foreach ($transacciones as $key) { 
			$saldo = $saldo + $key['tra_entrada'] -$key['tra_salida'];
			?>
		<tr>
			<td><?php echo $key['tra_id']+1?></td>
			<td><?php echo $key['tra_concepto']?></td>
			<td><?php echo $key['tra_comprobante']?></td>
			<td><?php echo ($key['tra_entrada']>0) ? "(+)".$key['tra_entrada']:"(-)".$key['tra_salida'];?></td>
			<td><?php echo $saldo?></td>
			<td><?php echo date('d-m-Y', strtotime($key['tra_fecha']))?></td>
		</tr>
			<?php } ?>
	</tbody>
</table>		 
	