<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dbmetadata
{
    protected $ci;
    protected $db_name; //Nombre de la base de datos
    protected $db_driver; //Nombre del Driver de conexion
    protected $patron = "/([a-z]*)_id_ctr/"; //Expresion regular a comparar en campos de tabla

    public function __construct()
    {
        $this->ci =& get_instance();
        $this->ci->load->database();
        $this->getDBName();
        $this->getDBDriver();
        //$this->checkTablas();
    }

    /**
     * Obtiene el nombre de la base de datos seleccionada por el usuario
     * @return string dbname
     */
    private function getDBName()
    {
        $this->db_name = $this->ci->db->database;
        return $this->db_name;
    }

    /**
     * Obtiene el nombre del driver de conexion seleccionado por el usuario
     * @return string dbdriver
     */
    private function getDBDriver()
    {
        $this->db_driver = $this->ci->db->platform();
        return $this->db_driver;
    }

    /**
     * Genera la consulta adecuada para obtener el listado de tablas 
     * pertenecientes a la base de datos seleccionada dependiendo 
     * del driver de conexion utilizado
     * @return string sql query
     */
    private function getQueryListadoTablas()
    {
        //Inicializar variable local
        $query = '';

        //Verificar que driver se esta utilizando
        switch ( $this->db_driver ) {
            case 'sqlsrv':
                $query = "SELECT name AS nombre_tabla FROM sys.objects WHERE type = 'u'";
                break;
            case 'mysql':
                $query = "SELECT table_name AS nombre_tabla 
                            FROM INFORMATION_SCHEMA.TABLES 
                            WHERE table_schema = '$this->db_name'";
                break;
            case 'mysqli':
                $query = "SELECT table_name AS nombre_tabla 
                            FROM INFORMATION_SCHEMA.TABLES 
                            WHERE table_schema = '$this->db_name'";
                break;
            default:
                $query = false;
                break;
        }

        return $query;
    }

    /**
     * Obtiene el listado de tablas pertenecientes a la base 
     * de datos seleccionada 
     * @return object listado tablas
     */
    public function getTablas()
    {
        $query_listado_tablas = $this->getQueryListadoTablas();
        
        if ( $query_listado_tablas == false ) {
            trigger_error("DB Driver no soportado, verifica el driver de conexion seleccionado", E_USER_WARNING);
        }

        $tablas = $this->ci->db->query( $query_listado_tablas );    
        
        return ( is_object( $tablas ) && $tablas->num_rows() > 0 )
                ? $tablas
                : false;
    }

    /**
     * Obtiene los nombres de las columnas pertenecientes a una tabla especifica
     * @param  string $nombre_tabla Nombre de la tupla
     * @return sql query columnas
     */
    public function getColumns( $nombre_tabla = '' )
    {
        //Inicializar variable local
        $query = '';

        if ( $nombre_tabla == '' ) {
            trigger_error("Debes especificar el nombre de la tabla", E_USER_WARNING);
            exit();
        } 

        //Verificar que driver se esta utilizando
        switch ( $this->db_driver ) {
            case 'sqlsrv':
                $query = "SELECT name AS nombre_columna FROM sys.columns WHERE object_id = OBJECT_ID('$nombre_tabla')";
                break;
            case 'mysql':
                $query = "SELECT COLUMN_NAME AS nombre_columna
                            FROM INFORMATION_SCHEMA.COLUMNS 
                            WHERE TABLE_SCHEMA ='$this->db_name' 
                            AND TABLE_NAME = '$nombre_tabla'";
                break;
            case 'mysqli':
                $query = "SELECT COLUMN_NAME AS nombre_columna
                            FROM INFORMATION_SCHEMA.COLUMNS 
                            WHERE TABLE_SCHEMA ='$this->db_name' 
                            AND TABLE_NAME = '$nombre_tabla'";
                break;
            default:
                $query = false;
                break;
        }

        return $query;
    }

    /**
     * Verificar si es posible utilizar la tabla seleccionada en 
     * un flujo de trabajo determinado (proceso)
     * @param  string $nombre_tabla nombre de la tabla
     * @return boolean True, es posible. False, no es posible
     */
    private function esTablaProceso( $nombre_tabla = '' )
    {
        //Generar query para obtener listado de columnas
        $query_listado_columnas = $this->getColumns( $nombre_tabla );
        //Obtener listado de columnas
        $columnas = $this->ci->db->query( $query_listado_columnas );

        //Inicializar respuesta
        $valido = false;

        //Recorer todos los campos
        foreach ( $columnas->result() as $columna ) {
            //Verificar si el nombre del campo posee el sufijo determinado de control
            if( preg_match($this->patron, $columna->nombre_columna, $matches)){
                $valido = true;
            }
        }

        return $valido;
    }

    /**
     * Inserta el listado de tablas pertenecientes a la base de datos en 
     * la entidad tab_tabla para ser utilizadas en la creacion de procesos 
     * personalizados
     */
    protected function setTablas()
    {
        //Obtener listado de tablas
        $tablas = $this->getTablas();
        //Verificar si la base de datos posee tablas
        if ( is_object( $tablas ) && $tablas->num_rows() > 0 ) {
            //Recorrer listado de tablas
            foreach ( $tablas->result() as $tabla ) {
                //inicializar variable
                $nombre_tabla = $tabla->nombre_tabla;
                //Verificar si es una tabla que puede ser usada en proceso
                $es_valida = $this->esTablaProceso( $nombre_tabla );
                //Guardar listado de tablas de control en la base de datos
                if ( $es_valida == true && $tabla->nombre_tabla != 'tab_tabla' ) {
                    //insertar nombre de tabla en la entidad tab_tablas
                    $this->ci->db->insert(
                                    'tab_tabla',
                                    array( 'tab_nombre' => $tabla->nombre_tabla )
                                );
                }
            }
        }
    }

    /**
     * Verifica si el listado de tablas correspondiente a la
     * base de datos seleccionada ya fueron ingresadas en la entidad tab_tabla
     */
    public function checkTablas()
    {
        $listado_tablas = $this->ci->db->get('tab_tabla');
        //Verificar si la entidad tab_tabla esta vacia, sino llenarla.
        if ( is_object( $listado_tablas ) && $listado_tablas->num_rows() <= 0 ){
            $this->setTablas();
        }
    }

    /**
     * Obtiene los nombres de los campos, tablas y columnas pertenecientes
     * a un llave foranea en una tabla especifica
     * @param  string $nombre_tabla nombre de la tabla
     * @return sql query obtener FK
     */
    public function getForeignKeys( $nombre_tabla )
    {
        //Inicializar variable local
        $query = '';

        if ( $nombre_tabla == '' ) {
            trigger_error("Debes especificar el nombre de la tabla", E_USER_WARNING);
            exit();
        } 

        //Verificar que driver se esta utilizando
        switch ( $this->db_driver ) {
            case 'sqlsrv':
                $query = "SELECT OBJECT_NAME(f.parent_object_id) AS tabla,
                                COL_NAME(fc.parent_object_id, fc.parent_column_id) AS columna,
                                OBJECT_NAME (f.referenced_object_id) AS tabla_referencia,
                                COL_NAME(fc.referenced_object_id, fc.referenced_column_id) AS columna_referencia
                            FROM sys.foreign_keys AS f
                            INNER JOIN sys.foreign_key_columns AS fc
                            ON f.OBJECT_ID = fc.constraint_object_id
                            WHERE f.parent_object_id = OBJECT_ID('$nombre_tabla')";
                break;
            case 'mysql':
                $query = "SELECT COLUMN_NAME AS nombre_columna
                            FROM INFORMATION_SCHEMA.COLUMNS 
                            WHERE TABLE_SCHEMA ='$this->db_name' 
                            AND TABLE_NAME = '$nombre_tabla'";
                break;
            case 'mysqli':
                $query = "SELECT COLUMN_NAME AS nombre_columna
                            FROM INFORMATION_SCHEMA.COLUMNS 
                            WHERE TABLE_SCHEMA ='$this->db_name' 
                            AND TABLE_NAME = '$nombre_tabla'";
                break;
            default:
                $query = false;
                break;
        }

        return $query;
    }

    public function checkForeignKeys( $nombre_tabla = '' )
    {
        $query_foreign_keys =  $this->getForeignKeys( $nombre_tabla );
        
    }


}

/* End of file dbmetadata.php */
/* Location: ./application/libraries/dbmetadata.php */
