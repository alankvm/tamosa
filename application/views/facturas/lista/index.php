<form action="<?=base_url('facturas/lista/seleccionar')?>" method="POST"  id="facturas_seleccionadas">
	
		<button type="submit" class="btn btn-primary" id="asignar" style="position: absolute; right: 27px; z-index: 100; top: 21%;">Asignar Responsable</button>
	
</form>
<table cellpadding="0" cellspacing="0" border="0" class="responsive dynamicTable display table table-bordered" width="100%">
	<thead>
		<tr>
			<th>
				Id
			</th>
			<th>
				Documento
			</th>	
			<th>
				Cliente
			</th>
			<th>
				Valor
			</th>
			<th>
				Acciones
			</th>
		</tr>
	</thead>		
	<tbody>		
		<?php foreach ($facturas as $key) { ?>
		<tr>
			<td>
				<?php echo $key['id']?>
			</td>				
			<td>
				<?php echo $key['documento']?>
			</td>
			<td>
				<?php echo $key['cliente']?>
			</td>
			<td>
				<?php echo $key['valor']?>
			</td>
			<td>
				<input type="checkbox" name="facturas[]" value="<?=$key['id']?>">
			</td>
		</tr>
		<?php } ?>
	</tbody>
</table>


<script src="<?=base_url('js/facturas/lista.js')?>" type="text/javascript"></script>