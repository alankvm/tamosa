function cargar_historial(id) {
	var url = "http://localhost/sistema/facturas/responsable/cargar_historial";
	$("#tabla_historial").html('');
	$.ajax({
				url: url,				
				data: {id:id},
				type: "POST",
				success: function (data) {
					$("#tabla_historial").html(data);
				}
			});
}

function cargar_recorrido(id) {
	var url = "http://localhost/sistema/facturas/carga/recorrido";
	$("#tabla_historial").html('');
	$.ajax({
				url: url,				
				data: {id:id},
				type: "POST",
				success: function (data) {
					$("#tabla_historial").html(data);
				}
			});
}

function capturar_valores(id, monto)
{
    $('#fac_id').attr('value',id);
    $('#monto').attr('value',monto);
}

var asignacion = {
	fn: {
		asignarRecorridos: function () {
			var elementos = $("input[name='pedidos[]']"),
				inputHidden = "",
				formElementos = $("#seleccionados");

			$.each(elementos, function (index, element) {
				if ($(element).is(":checked")) {
					inputHidden += '<input type="hidden" name="facturas[]" value="' + $(element).val() + '">';
				}
			});
			 formElementos.append(inputHidden);
			//return false;
		}
	},
	onReady: function () {
		$("#elementos").on("submit", asignacion.fn.asignarRecorridos);
		$("a[name='regresar[]']").on("click", function (event) {
			event.preventDefault();
			var that = $(this),
				datos = {
					id: that.data('id')
				},
				url = gSateliteWhite.baseUrl('facturas/responsable/regresar');
			$.ajax({
				async: false,
				url: url,
				data: datos,
				dataType: "JSON",
				type: "POST",
				success: function (data) {
					if (data === 1) {
						that.parent().parent().remove();
					}
				}
			});
		});
		
	},
	init: function () {
		asignacion.onReady();
	}
};

jQuery(document).ready(function($) {
	asignacion.init();
});