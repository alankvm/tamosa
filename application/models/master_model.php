<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Master_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        
    }

    public function getPeriodoDetalle()
    {
        return $this->getFechaActual()->format('d/m/Y');
    }

    public function getFechaActual()
    {
        $fecha = new DateTime;
        return $fecha;
    }

}

/* End of file master_model.php */
/* Location: ./application/models/master_model.php */