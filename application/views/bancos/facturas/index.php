<table cellpadding="0" cellspacing="0" border="0" class="responsive dynamicTable display table table-bordered" width="100%">
	<thead>
		<tr>
			<th>
				Id
			</th>
			<th>
				Documento
			</th>	
			<th>
				Cliente
			</th>
            <th>
                Vendedor
            </th>
            <th>
				Monto
			</th>
            <th>
                Abono
            </th>
            <th>
                Pendiente
            </th>
			<th>
				Acciones
			</th>
		</tr>
	</thead>		
	<tbody>		
		<?php foreach ($facturas as $key) { ?>
		<tr>
			<td>
				<?php echo $key['fac_id']?>
			</td>				
			<td>
				<?php echo $key['documento']?>
			</td>
			<td>
				<?php echo $key['cliente']?>
			</td>			
            <td>
                <?php echo $key['Codigo'].' - '.$key['Vendedor_nombre']?>
            </td>
            <td>
                <?php echo $key['fac_monto']?>
            </td>
            <td>
                <?php echo $key['abono']?>
            </td>
            <td>
                <?php echo $key['fac_monto']-$key['abono']?>
            </td>
			<td>
				<a data-toggle="modal" href="#asignar" data-id="<?php echo $key['fac_id'] ?>" onclick="javascript:$('#fac_id').attr('value',$(this).attr('data-id'))" title="Asignar monto recibido"><span aria-hidden="true" class="icon16 icomoon-icon-coins"></span></a> |                 
                <a data-toggle="modal" href="#historial" data-id="<?php echo $key['fac_id'] ?>" onclick="cargar_historial(<?php echo $key['fac_id'] ?>)" title="Ver historial"><span aria-hidden="true" class="minia-icon-calendar-2"></span></a> |                 
			</td>
		</tr>
		<?php } ?>
	</tbody>
</table>
<div class="modal fade hide" id="asignar">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h3>Asignar monto</h3>
    </div>
    <div class="modal-body">
    	<div class="box">            
            <div class="content">               
                <form class="form-horizontal" method="POST">
                    <div class="form-row row-fluid">
                        <div class="span12">
                            <div class="row-fluid">
                                <label class="form-label span4" for="nombres">Monto</label>
                                <input class="span8 text" id="monto" type="text" name="monto">
                            </div>
                        </div>
                    </div>
 					<div class="form-row row-fluid">
                        <div class="span12">
                            <div class="row-fluid">
                                <label class="form-label span4" for="nombres">Observaciones</label>
                                <textarea class="span8 text" id="obs" type="text" name="obs"></textarea>
                            </div>
                        </div>
                    </div>					
					<div class="form-actions">
                        <input type="hidden" id="fac_id" name="fac_id">
                       <button type="submit" class="btn btn-info">Guardar</button>
                       <button type="button" class="btn">Cancelar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal">Cerrar</a>
    </div>
</div>

<div class="modal fade hide" id="historial">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h3>Historial</h3>
    </div>
    <div class="modal-body">
        <div class="box">
            <div class="content">               
                <div id="tabla_historial"></div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal">Cerrar</a>
    </div>
</div>
