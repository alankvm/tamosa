var query = {
    fn: {
        llenarListaCampos: function (rows) {
            //Inicializar variables
            var $listaCampos = $("#campos"),
                opciones = "<option></option>";

            //Recorrer listado de resultados enviados desde el servidor
            $.each(rows, function (index, element) {
                opciones += "<option value='" + element.nombre_columna + "'>" + element.nombre_columna + "</option>";
            });
            if (!$("div.generador").is(":visible")) {
                $("div.principal").toggle(200);
                $("div.generador").toggle(400);
            }
            //Limpiar resultados anteriores (si existen)
            $listaCampos.empty();
            //Llenar select con el nuevo lisatdo
            setTimeout(function () {
                $listaCampos.select2("destroy").append(opciones).select2().select2("open");
            }, 400);
        },
        agregarCampo: function (event) {
            event.preventDefault();
            var $dropDownCampos = $("select#campos"),
                $textAlias = $("input#alias"),
                $displayFields = $("span.display-fields"),
                esFiltro = $("input#filtro").is(":checked"),
                tabla = $("select#tablas").find(":selected").val(),
                field = ($displayFields.text() === "*") ? "" : $displayFields.html() + ",\n",
                datos = {
                    campo: $("select#campos").find(":selected").val(),
                    tabla: tabla,
                    alias: $textAlias.val()
                };

            //Verificar validacion 
            if (validacion.fn.verificarAgregarCampo() === true) {
                field += tabla + "." + $dropDownCampos.find(":selected").val();
                field += " <span class='red'>AS</span> " + "<span class='blue'>" + $textAlias.val() + "</span>";
                query.fn.efectoTransicionContenedorQuery();

                //limpiar campos
                $textAlias.val("");
                //Remover columna del DropDownList
                $dropDownCampos.find(":selected").remove();
                $dropDownCampos.select2("data", null);
                //Abrir DropDown para capturar el siguiente campo
                $dropDownCampos.select2("open");
                //verificar si el campo fue seleccionado para ser filtro
                if (esFiltro === true) {
                    query.fn.mostrarListadoFiltros(datos);
                }
                //Agregar al query
                $displayFields.html(field);
            }

            return true;
        },
        llenarListaForaneas: function (rows) {
            //Inicializar variables
            var $dropDownForaneas = $("#foraneas"),
                listaForaneas = "<option></option>",
                opcion = "",
                value = "";
            if (rows !== null) {
                $.each(rows, function (index, element) {
                    //preparar opcion
                    opcion = element.tabla + "." + element.columna;
                    opcion += " - a - " + element.tabla_referencia;
                    opcion += "." + element.columna_referencia;
                    //preparar value
                        //Array ( tablaPadre| campoPadre| TablaReferencia| ColumnaReferencia)
                    value = element.tabla + "|" + element.columna + "|";
                    value += element.tabla_referencia + "|" + element.columna_referencia;
                    //llenar opcion
                    listaForaneas += "<option value='" + value + "'>" + opcion + "</option>";
                });
            } else {
                listaForaneas += "<option>Cero llaves Foraneas Encontradas</option>";
                //Deshabilitar el uso del dropdown foraneas
                $dropDownForaneas.prop('disabled', 'disabled');
            }

            //Limpiar resultado anteriores si existen
            $dropDownForaneas.empty();

            return $dropDownForaneas.append(listaForaneas);
        },
        llenarListaCamposForaneos: function (rows) {
            //Inicializar variables
            var $dropDownCamposForaneos = $("#foreignkeys"),
                listaCamposForaneos = "";
            //Recorrer listado de resultados enviados desde el servidor
            $.each(rows, function (index, element) {
                listaCamposForaneos += "<option value='" + element.nombre_columna + "'>" + element.nombre_columna + "</option>";
            });
            //Limpiar resultados anteriores si existen
            $dropDownCamposForaneos.empty();
            $dropDownCamposForaneos.select2("destroy");
            $dropDownCamposForaneos.select2();
            $dropDownCamposForaneos.select2("open");
            return $dropDownCamposForaneos.append(listaCamposForaneos);
        },
        mostrarListadoFiltros: function (datos) {
            var $divQuery = $("div#query"),
                $divListadoFiltros = $("div#listado-filtros"),
                $tablaFiltros =  $("table tbody.filtros"),
                numeroFila = $tablaFiltros.children().length + 1,
                fila = "",
                hiddenFiltro = "";

            if (!$divListadoFiltros.is(":visible")) {
                //Animar divQuery
                $divQuery.switchClass("span12", "span7", 800);
                //Animar divListado de Filtros y mostrar
                setTimeout(function () { $divListadoFiltros.toggle(500); }, 500);
            }
            hiddenFiltro = datos.tabla + "." + datos.campo + '|' + datos.alias;
            fila += '<tr class="info">';
            fila += '<td>' + numeroFila + '</td>';
            fila += '<td>' + datos.tabla + "." + datos.campo + '</td>';
            fila += '<td>' + datos.alias;
            fila += '<input type="hidden" name="filtros[]" value="' + hiddenFiltro + '">';
            fila += '</td>';
            fila += '</tr>';

            return $tablaFiltros.append(fila);
        },
        MostrarListadoRelaciones: function (event) {
            event.preventDefault();
            var $divRelaciones = $("div.relaciones"),
                $divListadoCampos = $("div.fields"),
                datos = {
                    tabla: query.fn.getTablaBase()
                };

            if ($divListadoCampos.is(":visible")) {
                //Obtener listado de relaciones asociadas a la tabla base seleccionada
                query.ajax.getRelaciones(datos);
                //Esconder Div con listado de campos 
                $divListadoCampos.toggle(400);
                //Esperar un momento y mostrar el div con las relaciones
                setTimeout(function () { $divRelaciones.toggle(400); }, 400);
            }

        },
        MostrarListadoCampos: function (event) {
            event.preventDefault();
            var $divRelaciones = $("div.relaciones"),
                $divListadoCampos = $("div.fields");

            if ($divRelaciones.is(":visible")) {
                //Mostrar Div con listado de campos 
                $divRelaciones.toggle(400);
                //Esperar un momento y ocultar el div con las relaciones
                setTimeout(function () { $divListadoCampos.toggle(400); }, 400);
            }
        },
        agregarRelacion: function (event) {
            event.preventDefault();
            var $dropDownCampos = $("select#foreignkeys"),
                $textAlias = $("input#foreign_alias"),
                $displayFields = $("span.display-fields"),
                esFiltro = $("input#foreign_filtro").is(":checked"),
                tabla = $("select#foraneas").find(":selected").val().split("|").reverse()[1],
                field = ($displayFields.text() === "*") ? "" : $displayFields.html() + ",\n",
                datos = {
                    campo: $("select#foreignkeys").find(":selected").val(),
                    tabla: tabla,
                    alias: $textAlias.val()
                };

            //Verificar validacion de relaciones
            if (validacion.fn.verificarAgregarRelacion() === true) {
                field += tabla + "." + $dropDownCampos.find(":selected").val();
                field += " <span class='red'>AS</span> " + "<span class='blue'>" + $textAlias.val() + "</span>";
                query.fn.efectoTransicionContenedorQuery();

                //limpiar campos
                $textAlias.val("");
                //Remover columna del DropDownList
                $dropDownCampos.find(":selected").remove();
                //Limpiar dropdown
                $dropDownCampos.select2("data", null);
                //Abrir DropDown para capturar el siguiente campo
                $dropDownCampos.select2("open");

                //Agregar JOIN
                query.fn.agregarJoin();
                //verificar si el campo fue seleccionado para ser filtro
                if (esFiltro === true) {
                    query.fn.mostrarListadoFiltros(datos);
                }
                $displayFields.html(field);
            }

            return true;
        },
        agregarJoin: function () {
            var $dropDownForaneas = $("select#foraneas"),
                tabla = $dropDownForaneas.find(":selected").val().split("|").reverse()[1],
                relacion = $dropDownForaneas.find(":selected").text().split("-"),
                join = "<span class='red'>JOIN </span>" + tabla + "\n<span class='red'>ON</span>" + relacion[2] + " = " + relacion[0],
                $displayFields = $("span.joins"),
                field = ($displayFields.text() === "") ? "" : $displayFields.html() + "\n";

            field += join;
            query.fn.efectoTransicionContenedorQuery();

            //Verificar si la relacion ya fue agregada
            if (typeof $dropDownForaneas.find(":selected").data("agregada") == 'undefined') {
                //Agregar bandera de agregada
                $dropDownForaneas.find(":selected").data("agregada", "true");
                //Agregar relacion
                $displayFields.html(field);
            }

            return true;
        },
        cambiarTablaSeleccionada: function (datos) {
            return $("span.selected-table").text(datos.tabla);
        },
        efectoTransicionContenedorQuery: function () {
            return $("pre.query").effect("pulsate", 35);
        },
        getTablaBase: function () {
            return $("select#tablas").find(":selected").val();
        },
        enviarFormularioConsulta: function (event) {
            event.preventDefault();
            var nombre = $("input#nombre").val(),
                descripcion = $("input#descripcion").val(),
                query = $("pre.query").text().trim(),
                filtrosRows = $("input[name='filtros[]'"),
                $divHiddenFilters = $("div.inputfilters");

            if (filtrosRows.length > 0) {
                $.each(filtrosRows, function (index, element) {
                    $divHiddenFilters.append(element);
                });
            }

            //Set values a los campos escondidos
            $("input#hiddenName").val(nombre);
            $("input#hiddenDescription").val(descripcion);
            $("input#hiddenQuery").val(query);

            return $("form#generarQuery").submit();
        },
        mostrarResumenFinalizar: function (event) {
            event.preventDefault();
            var $divFinalizar = $("div.finalizar"),
                $nombreReporte = $("input#nombre"),
                $descripcionReporte = $("input#descripcion"),
                $divRelaciones = $("div.relaciones"),
                $divListadoCampos = $("div.fields");

            //Construir Resumen
            $("dd#rptname").text($nombreReporte.val());
            $("dd#rptdesc").text($descripcionReporte.val());

            if ($divListadoCampos.is(":visible")) {
                $divListadoCampos.toggle(400);
            }

            if ($divRelaciones.is(":visible")) {
                $divRelaciones.toggle(400);
            }

            setTimeout(function () {
                $divFinalizar.toggle(400);
            }, 400);
        }
    },
    ajax: {
        getColumnas: function () {
            var url = gSateliteWhite.baseUrl("sistema/procesos/getColumnas"),
                datos = {
                    tabla: $(this).val()
                };
            //cambiar nombre de la tabla base
            query.fn.cambiarTablaSeleccionada(datos);
            query.fn.efectoTransicionContenedorQuery();

            //obtener listado de campos
            $.ajax({
                url: url,
                dataType: 'JSON',
                type: 'GET',
                data: datos,
                success: function (data) {
                    //Llenar lista de Campos
                    if (validacion.fn.verificarNuevoReporte() === true ) {
                        query.fn.llenarListaCampos(data);    
                    }
                    
                }
            });
        },
        getRelaciones: function (datos) {
            var url = gSateliteWhite.baseUrl("sistema/procesos/getRelaciones");
            $.ajax({
                url: url,
                dataType: 'json',
                type: 'GET',
                data: datos,
                success: function (data) {
                    //Llenar DropDownList Llaves Foraneas
                    query.fn.llenarListaForaneas(data);
                }
            });
        },
        getColumnasForaneas: function () {
            //Obtener nombre de la tabla relacion
            /*
                Cargar lista de campos correspondientes a la tabla relacion
                Buscar option selected, obtener value, convertir a array,
                invertir el orden del array y obtener posicion 1
             */
            var url = gSateliteWhite.baseUrl("sistema/procesos/getColumnas"),
                datos = {
                    tabla: $(this).find(":selected").val().split("|").reverse()[1]
                };

            $.ajax({
                url: url,
                dataType: 'json',
                type: 'GET',
                data: datos,
                success: function (data) {
                    //Llenar lista de campos Foraneos
                    query.fn.llenarListaCamposForaneos(data);
                }
            });
        }
    },
    onReady: function () {
        //Inicializar Select2
        $("select").select2();
        //Paso 1 - Obtener campos de la tabla seleccionada
        $("select#tablas").on("change", query.ajax.getColumnas);
        //Paso 2 - Agregar campos a la consulta
        $("button.agregar-field").on("click", query.fn.agregarCampo);
        //Mostrar listado de relaciones
        $("button.relations").on("click", query.fn.MostrarListadoRelaciones);
        //Obtener listado de columnas foraneas 
        $("select#foraneas").on("change", query.ajax.getColumnasForaneas);
        //Paso 3 - Agregar campos de relacion a la consulta
        $("button.agregar-relacion").on("click", query.fn.agregarRelacion);
        //Regresar a la lista de campos de la tabla base
        $("button.regresar").on("click", query.fn.MostrarListadoCampos)
        //Paso 4 - Guardar consulta generada
        $("button[type='submit']").on("click", query.fn.enviarFormularioConsulta);
        //Esconder el div de relaciones 
        $("div.relaciones").toggle();
        //Esconder el div de listado de filtros
        $("div#listado-filtros").toggle();
        //Esconder generador de consultas
        $("div.generador").toggle();
        //Esconder finalizar consulta
        $("div.finalizar").toggle();
        //Desplegar resumen y finalizar
        $("button.finalizar").on("click", query.fn.mostrarResumenFinalizar);
    },
    init: function () {
        query.onReady()
    }
};
jQuery(document).ready(function ($) {
    query.init();
});