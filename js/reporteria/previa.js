var previa = {
    fn: {
        agregarFiltro: function (event) {
            event.preventDefault();
            //inicializar variables
            var $dropDownFiltros = $("select#filtros"),
                $tablaFiltros = $("table tbody.filtros"),
                $dropDownOperadores = $("select#operadores"),
                $textConfrontar = $("input#confrontar"),
                fila = "",
                inputHidden = "",
                $dropDownConcatenador = $("select#contenador"),
                concatenar = $("div.concatenador").is(":visible"),
                resultadoConcatenador = (concatenar === true) ?  $dropDownConcatenador.find(":selected").val() : "No Aplica",
                comandoSQL = "";

            //Generando Comando SQL
            comandoSQL += $dropDownFiltros.find(":selected").val() + ' ';
            comandoSQL += $dropDownOperadores.find(":selected").val() + ' ';
            comandoSQL += $textConfrontar.val();

            //Verificar si aplicar el operador concatenador y construir el input con los valores a utilizar
            if (concatenar === false) {
                inputHidden += '<input type="hidden" name="active_filters[]" value="';
                inputHidden += $dropDownFiltros.find(":selected").val() + ' ';
                inputHidden += $dropDownOperadores.find(":selected").val() + ' ';
                inputHidden += $textConfrontar.val();
                inputHidden += '">';
            } else {
                inputHidden += '<input type="hidden" name="active_filters[]" value="';
                inputHidden += resultadoConcatenador + ' ';
                inputHidden += $dropDownFiltros.find(":selected").val() + ' ';
                inputHidden += $dropDownOperadores.find(":selected").val() + ' ';
                inputHidden += $textConfrontar.val();
                inputHidden += '">';
            }

            //Generar nueva fila a insertar
            fila += '<tr>';
            fila += '<td>' + $dropDownFiltros.find(":selected").text() + '</td>';
            fila += '<td>' + $dropDownOperadores.find(":selected").text() + '</td>';
            fila += '<td>' + $textConfrontar.val() + '</td>';
            fila += '<td>' + resultadoConcatenador + '</td>';
            fila += '<td><pre class="prettyprint linenums" style="padding: 2px; margin-bottom: auto;">';
            fila += comandoSQL;
            fila += '</pre></td>';
            fila += '<td>';
            fila += '<div class="controls center">';
            fila += '<a href="#" class="tip eliminar" oldtitle="Tooltip in the top" title="Eliminar">';
            fila += '<span class="icon12 icomoon-icon-remove"></span></a>';
            fila += '</div>';
            fila += inputHidden;
            fila += '</td>';
            fila += '</tr>';

            $tablaFiltros.append(fila);
            //Verificar si se pueden ver los detalles del filtro
            if (!$("div.active-filters").is(":visible")) {
                previa.fn.mmostrarOcultarDetalleFiltros();
            } else {
                $tablaFiltros.effect("highlight");
            }
            //Activar selector de concatenador para el siguiente filtro
            previa.fn.activarConcatenador();

        },
        mmostrarOcultarDetalleFiltros: function () {
            return $("div.active-filters").toggle(400);
        },
        activarConcatenador: function () {
            var $divConcatenador = $("div.concatenador"),
                $tablaFiltros = $("table tbody.filtros");

            if ($tablaFiltros.children().length > 0 && $divConcatenador.is(":visible") === false) {
                $divConcatenador.toggle(400);
            }
        },
        minimizarGeneracionFiltros: function () {
            // Inicializacion de variables
            var widgetClose = $('div.box.closed');
            if (widgetClose.find('div.content').is(":visible")) {
                //close all widgets with class "closed"
                widgetClose.find('div.content').toggle(200);
                widgetClose.find('.title>.minimize').removeClass('minimize').addClass('maximize');
            }

            return true;
        },
        eliminarFiltro: function (event) {
            event.preventDefault();
            var $this = $(this),
                $fila = $this.parent().parent().parent(),
                esFiltroBase = $fila.is(":first-child");
            if (esFiltroBase === true) {
                $.pnotify({
                    type: 'info',
                    title: 'informaci&oacute;n',
                    text: 'No puedes eliminar el filtro base.',
                    icon: 'picon icon16 brocco-icon-info white',
                    opacity: 0.95,
                    history: false,
                    sticker: false
                });
            } else {
                $fila.remove();
                $.pnotify({
                    type: 'success',
                    title: 'Operaci&oacute;n Completada!',
                    text: 'Filtro eliminado satisfactoriamente.',
                    icon: 'picon icon16 iconic-icon-check-alt white',
                    opacity: 0.95,
                    history: false,
                    sticker: false
                });
            }
        }

    },
    ajax: {
        filtrarResultados: function (event) {
            event.preventDefault();
            //Inicializando Variables
            var url = gSateliteWhite.baseUrl("reporteria/qmanager/filtrar"),
                datos = {
                    filtro: "WHERE ",
                    consulta: $("input[name=id_consulta").val()
                },
                $filtros = $("input[name='active_filters[]']");

            //Formando Query para filtrado
            $filtros.each(function (index, element) {
                datos.filtro += $(element).val() + ' ';
            });

            //Almacenar filtros
            $("input[name=pdf_filtros]").val(datos.filtro);

            $.ajax({
                async: false,
                url: url,
                dataType: 'HTML',
                type: 'POST',
                data: datos,
                success: function (data) {
                    //Mostrando resultados del filtrado
                    $("table tbody.resultado").empty().html(data).parent().effect("pulsate", 100);
                    //Esconder el generador de filtros para mayor rango visual
                    previa.fn.minimizarGeneracionFiltros();

                }
            });
        },
        removerFiltroResultados: function (event) {
            event.preventDefault();
            //Inicializando Variables
            var url = gSateliteWhite.baseUrl("reporteria/qmanager/filtrar"),
                datos = {
                    filtro: "",
                    consulta: $("input[name=id_consulta]").val()
                };

            //Remover filtros
            $("input[name=pdf_filtros]").val("");

            $.ajax({
                async: false,
                url: url,
                dataType: 'HTML',
                type: 'POST',
                data: datos,
                success: function (data) {
                    //Mostrando resultados del filtrado
                    $("table tbody.resultado").empty().html(data).parent().effect("pulsate", 100);
                    //Esconder el generador de filtros para mayor rango visual
                    previa.fn.minimizarGeneracionFiltros();
                }
            });
        }
    },
    onReady: function () {
        //Agregar Filtro
        $("button.agregar").on("click", previa.fn.agregarFiltro);
        //Eliminar Filtro
        $("table tbody.filtros").on("click", "a", previa.fn.eliminarFiltro);
        //Aplicar Filtro(s)
        $("#filtrado").on("submit", previa.ajax.filtrarResultados);
        //Remover Filtro(s)
        $("button.remover").on("click", previa.ajax.removerFiltroResultados);

    },
    init: function () {
        previa.onReady();
    }
};

jQuery(document).ready(function ($) {
    previa.init();
});