<div class="header">
	<div class="logo">
		<img src="<?=base_url('media/logo/LOGOTAMOSA.png')?>" alt="">
	</div>
	<div class="leyenda">
		<h3>Control de Documentos en Ruta</h3>
	</div>
</div>
<?php foreach ($tipo_documento as $key) { ?>
<table>
	<thead>
		<tr><th colspan="3"><?php print_r($key['tid_nombre']); ?></th></tr>
		<tr>			
			<th>Documento</th>
			<th>Cliente</th>
			<th>Valor</th>			
		</tr>
	</thead>
	<tbody>
		<?php $total = 0; ?>
		<?php foreach ( $listado as $factura ) :
		if ($key['tid_id'] == $factura['tid_id']) {
		?>
		<?php $total = $total + $factura['valor'];?>
		<tr>
			<td><?php echo $factura['documento']; ?></td>
			<td><?php echo $factura['cliente']; ?></td>
			<td><?php echo $factura['valor']; ?> </td>			
		</tr>
		<?php }
		endforeach; ?>
	</tbody>
	
</table>
<?php } ?>