<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class Proceso_model extends CI_Model
{

    /**
     * 
     * Inicializar variables y cargar librerias
     */
    public function __construct()
    {
        $this->load->database();
        //Inicializar variables
        $this->db_driver = $this->db->platform();
        $this->db_name = $this->db->database;
        $this->load->library('dbmetadata');
    }

    /*
      Inicia seccion de consultas de la libreria proceso
    */

    /**
     * Obtiene el nombre del rol perteneciente a un ID de rol especifico
     * @param  integer $id id del rol
     * @return string Nombre del rol
     */
    public function get_rol( $id )
    {
        $this->db->select()
        ->where( 'uxr_id_usu', $id)
        ->from( 'uxr_usuarioxrol' );

        $query = $this->db->get();
        return $query->row_array();
    }

    /**
     * Obtener flujo de proceso por ID de flujo
     * @param  integer $flujo ID del flujo
     * @return array detalles del flujo de proceso
     */
    public function get_proceso( $flujo )
    {
        $this->db->select()
        ->where( 'flu_nombre', $flujo )
        ->from( 'flu_flujo' )
        ->join( 'tab_tabla', 'tab_id=flu_id_tab' );

        $query = $this->db->get();
        return $query->row_array();
    }

    /**
     * Obtiene estado del flujo de proceso
     * @param  string $flujo nombre del flujo de proceso
     * @param  integer $estado ID del estado
     * @return array detalles del flujo de proceso
     */
    public function get_estado( $flujo, $estado )
    {
        $this->db->select()
        ->where( 'est_nombre',  $estado )
        ->where( 'exp_id_flu', $flujo )
        ->from( 'est_estado' )
        ->join( 'exp_estadoxproceso', 'exp_id_est=est_id' );

        $query = $this->db->get();
        return $query->row_array();
    }

    /**
     * Obtiene listado de campos relacionados a un flujo 
     * en especifico
     * @param  int $flujo ID de Flujo de Proceso
     * @return array listado de campos pertenecientes a un flujo especifico
     */
    public function get_campos( $flujo )
    {
        $this->db->select()
        ->where( 'flu_id' , $flujo )
        ->from( 'cam_campo' )
        ->join( 'tab_tabla', 'tab_id=cam_id_tab' )
        ->join( 'flu_flujo', 'flu_id_tab=tab_id' )
        ->order_by( 'cam_orden' );

        $query = $this->db->get();
        return $query->result_array();
    }

    /**
     * Obtiene listado de permisos por proceso, estado y rol
     * @param  integer $proceso ID del proceso
     * @param  integer $estado  ID del estado
     * @param  integer $rol     ID del rol
     * @return array listado de permisos pertenecientes al flujo especificado
     */
    public function get_permisos ($proceso, $estado, $rol )
    {
        $this->db->select()
        ->where( 'exp_id_flu', $proceso )
        ->where( 'exp_id_est', $estado )
        ->from( 'exp_estadoxproceso' )
        ->join( 'pxp_permisoxprocesoxrol', 'pxp_id_exp=exp_id AND pxp_id_rol='.$rol )
        ->join( 'per_permiso', 'per_id=pxp_id_per' );

        $query = $this->db->get();        
        return $query->result_array();
    }

    public function get_permisos_especiales ($proceso, $estado, $rol )
    {
        $this->db->select()
        ->where( 'exp_id_flu', $proceso )
        ->where( 'exp_id_est', $estado )
        ->from( 'exp_estadoxproceso' )
        ->join( 'exs_estadoxpermiso_especial', 'exs_id_exp=exp_id AND exs_id_rol='.$rol )
        ->join( 'pes_permiso_especial', 'pes_id=exs_id_pes' );

        $query = $this->db->get();        
        return $query->result_array();
    }

    /**
     * Obtiene listado de relaciones correspondientes a una tabla 
     * perteneciente a un flujo en especifico
     * @param  integer $flujo ID del flujo
     * @return array nombres de entidades y campos relacionados
     */
    public function get_relaciones( $flujo )
    {
        $this->db->select()
        ->where( 'flu_id', $flujo )
        ->from( 'rel_relacion' )
        ->join( 'tab_tabla', 'tab_id=rel_id_tab_padre' )
        ->join( 'cam_campo', 'cam_id=rel_id_cam_padre' )
        ->join( 'flu_flujo', 'flu_id_tab=tab_id' );

        $query = $this->db->get();        
        return $query->result_array();
    }

    /**
     * Obtiene listado de subflujos correspondientes a un 
     * flujo principal especifico
     * @param  int $flujo ID del flujo principal
     * @return array listado de subflujos correspondientes a un flujo especifico
     */
    public function get_subflujos( $flujo )
    {
        $this->db->select()
        ->where( 'flu_padre', $flujo )
        ->from( 'flu_flujo')
        ->join( 'exp_estadoxproceso', 'exp_id_flu=flu_id' )
        ->join( 'est_estado', 'est_id=exp_id_est' );

        $query = $this->db->get();        
        return $query->result_array();
    }

    /**
     * Obtiene ID del flujo procesos padre para un subflujo
     * @param  int $padre ID del flujo de procesos
     * @return array detalles del flujo principal
     */
    public function get_padre( $padre )
    {
        $this->db->select()
        ->where( 'flu_id', $padre )
        ->from( 'flu_flujo' )
        ->join( 'tab_tabla', 'tab_id=flu_id_tab' )
        ->join( 'cam_campo', 'cam_id=flu_referencia' );

        $query=$this->db->get();
        return $query->row_array();
    }

    public function get_condiciones($flujo, $estado)
    {
      $this->db->select()
      ->where( 'exp_id_flu', $flujo )
      ->where( 'exp_id_est', $estado )
      ->from( 'cnd_condicion' )
      ->join( 'exp_estadoxproceso', 'exp_id=cnd_id_exp' )
      ->join( 'cam_campo', 'cam_id=cnd_id_cam' );

      $query = $this->db->get();        
      return $query->result_array();
  }

    /**
     * Obtiene listado de flujos portenecientes a el modulo actual
     * @param  integer $flujo  ID del flujo
     * @param  integer $estado ID del estado
     * @return array opcion de menu 
     */
    public function modulo_actual($flujo,$estado)
    {
        $exp = $this->get_exp( $flujo, $estado );

        $opcion = $this->get_opc($exp['exp_id_opc']);

        if ( $opcion['opc_padre'] != 0 ) {
            $opcion = $this->get_opc( $opcion['opc_padre'] );
            if ( $opcion['opc_padre'] !=0 ) {
              $opcion = $this->get_opc($opcion['opc_padre']);
          }
      }
      return $opcion;
  }

    /**
     * Obtener opcion de menu 
     * @param  [type] $opcion [description]
     * @return [type]         [description]
     */
    public function get_opc( $opcion )
    {
        $this->db->select()
        ->where('opc_id',$opcion)
        ->from('opc_opcion');
        $query=$this->db->get();        
        return $query->row_array();
    }

    /**
     * Obtiene estados por flujo de proceso 
     * @param  integer $flujo ID de flujo de proceso
     * @param  integer $estado ID de estado
     * @return array listado de estados 
     */
    public function get_exp($flujo,$estado)
    {
      $this->db->select()
      ->where( 'exp_id_flu', $flujo )
      ->where( 'exp_id_est', $estado )
      ->from( 'exp_estadoxproceso' );

      $query = $this->db->get(); 
      return $query->row_array();
  }

    /*
      Finaliza seccion de consultas de la libreria proceso
     */


      public function get_registro($tabla, $id)
      {
          $campo = $this->_crear_id($tabla);
          $this->db->select()
          ->where($campo,$id)
          ->from($tabla);    
          $query=$this->db->get();        
          return $query->row_array();
      }

      public function get_historial($control)
      {      
          $this->db->select()
          ->where('con_id',$control['con_id'])
          ->from('his_historial')
          ->join('con_control','con_id=his_id_con')      
          ->join('users','id=his_id_usu')
          ->join('est_estado','est_id=his_id_est');
          $query=$this->db->get();        
          return $query->result_array();
      }

      public function get_control($id)
      {      
          $this->db->select()
          ->where('con_id',$id)
          ->from('con_control');    
          $query=$this->db->get();        
          return $query->row_array();
      }

      public function get_estado_actual($control)
      {
          if ($control['con_id_est']!=-1) {
              $this->db->select()
              ->where('exp_id_flu',$control['con_id_pro'])
              ->where('exp_estado',1)
              ->where('exp_orden',$control['con_id_est'])
              ->from('exp_estadoxproceso');
              $query=$this->db->get();        
              return $query->row_array();
          }
          else
              $orden['exp_orden']=0;
          return $orden;
      }

      public function get_proximo_estado($control)
      {
          $orden = $this->get_estado_actual($control);         
          $this->db->select()
          ->where('exp_id_flu',$control['con_id_pro'])
          ->where('exp_estado',1)
          ->where('exp_orden>',$orden['exp_orden'])
          ->from('exp_estadoxproceso')
          ->join('flu_flujo','flu_id=exp_id_flu')      
          ->join('est_estado','est_id=exp_id_est');
          $query=$this->db->get();        
          return $query->first_row();
      }

      public function get_anterior_estado($control)
      {
          $orden = $this->get_estado_actual($control);
          $this->db->select()
          ->where('exp_id_flu',$control['con_id_pro'])
          ->where('exp_estado',1)
          ->where('exp_orden<',$orden['exp_orden'])
          ->from('exp_estadoxproceso')
          ->join('flu_flujo','flu_id=exp_id_flu')      
          ->join('est_estado','est_id=exp_id_est');
          $query=$this->db->get();        
          return $query->first_row();
      }

      public function get_proximos_estados($control)
      {
          $orden = $this->get_estado_actual($control);
          $this->db->select()
          ->where('exp_id_flu',$control['con_id_pro'])
          ->where('exp_estado',1)
          ->where('exp_orden>',$orden['exp_orden'])
          ->from('exp_estadoxproceso')
          ->join('flu_flujo','flu_id=exp_id_flu')      
          ->join('est_estado','est_id=exp_id_est');
          $query=$this->db->get();        
          return $query->result_array();
      }

      public function get_anteriores_estados($control)
      {
          $orden = $this->get_estado_actual($control);
          $this->db->select()
          ->where('exp_id_flu',$control['con_id_pro'])
          ->where('exp_estado',1)
          ->where('exp_orden<',$orden['exp_orden'])
          ->from('exp_estadoxproceso')
          ->join('flu_flujo','flu_id=exp_id_flu')      
          ->join('est_estado','est_id=exp_id_est');
          $query=$this->db->get();        
          return $query->result_array();
      }

      public function set_control($info)
      {
          $control = array(
            'con_id_usu' => (int)$info['usuario'],
            'con_fecha' => $info['fecha'],
            'con_id_est' => (int)$info['estado']);

          $historial = array(
            'his_id_con' => (int)$info['control'],
            'his_id_est' => (int)$info['estado'],
            'his_id_usu' => (int)$info['usuario'],
            'his_fecha' => $info['fecha'],
            'his_observacion' => $info['observacion']);
          $this->upd_registro('con_control',$control,'con_id',(int)$info['control']);
          $this->add_registro('his_historial',$historial);
          return 1;
      }

      public function add_control($info)
      {
          $control = array(
            'con_id_usu' => (int)$info['usuario'],
            'con_fecha' => $info['fecha'],
            'con_id_est' => (int)$info['estado'],
            'con_id_pro' => (int)$info['flujo']);
          $con_id = $this->add_registro('con_control',$control);

          $historial = array(
            'his_id_con' => (int)$con_id,
            'his_id_est' => (int)$info['estado'],
            'his_id_usu' => (int)$info['usuario'],
            'his_fecha' => $info['fecha']);      
          $this->add_registro('his_historial',$historial);
          return $con_id;
      }

      public function upd_registro($tabla,$cadena,$campo,$id)
      {
          $this->db->where($campo, $id);
          $this->db->update($tabla, $cadena);
          return TRUE;
      }

      public function add_registro($tabla, $cadena)
      {
        $this->db->insert($tabla, $cadena);
        return $this->db->insert_id();
    }

    /**
     * Listado de tablas pertenecientes a la base de datos seleccionada
     * @return object listado de tablas
     */
    public function getListadoTablas()
    {
        $listado = $this->db->get( 'tab_tabla' );
        return ( is_object( $listado ) && $listado->num_rows() > 0 )
        ? $listado
        : false;
    }

    /**
     * Listado de permisos (acciones a realizar en el proceso)
     * @return object listado de permisos
     */
    public function getListadoPermisos()
    {
        $listado = $this->db->get('per_permiso');
        return ( is_object( $listado ) && $listado->num_rows() > 0 )
        ? $listado
        : false;
    }

    /**
     * Obtener columnas pertenecientes a una tabla en especifico
     * @param  string $nombre_tabla nombre de la tabla
     * @return object listado de campos
     */
    public function getColumnas( $nombre_tabla = '' )
    {
        $query_columnas = $this->dbmetadata->getColumns( $nombre_tabla );
        $listado = $this->db->query( $query_columnas );

        return ( is_object( $listado ) && $listado->num_rows() > 0 )
        ? json_encode( $listado->result() )
        : false;
    }

    /**
     * Obtiene listado de campos y tablas relacionados a una tabla 
     * en especifico
     * @param  string $nombre_tabla nombre de tabla
     * @return object listado de foreign keys y tablas relacionadas
     */
    public function getRelaciones( $nombre_tabla = '' )
    {
        $query_foreign_keys = $this->dbmetadata->getForeignKeys( $nombre_tabla );
        $listado = $this->db->query( $query_foreign_keys );

        return ( is_object( $listado ) && $listado->num_rows() > 0 )
        ? json_encode( $listado->result() )
        : false;
    }

    /**
     * Obtener listado de estados
     * @return object listado de estados del proceso
     */
    public function getEstados()
    {
        $listado = $this->db->get('est_estado');
        return ( is_object( $listado ) && $listado->num_rows() > 0 )
        ? $listado
        : false;
    }

    /**
     * Obtener listado de roles del sistema
     * @return object listado de roles
     */
    public function getRoles()
    {
        $listado = $this->db->get('rol_rol');
        return ( is_object( $listado ) && $listado->num_rows() > 0 )
        ? $listado
        : false;
    }

    private function _crear_id($tabla)
    {
        $cadena = explode("_", $tabla);
        $campo = $cadena[0].'_id';
        return $campo;
    }

    /**
     * Creacion de nuevo flujo de proceso
     * @param array $data listado de parametros establecidos desde procesos/index
     * @return boolean resultado de la insercion del proceso
     */
    public function setNuevoProceso( $data = array() )
    {
        //Inicializacion de variables 
        $validacion_flujo    = false;
        $validacion_relacion = false;
        $validacion_campos   = false;
        $validacion_permisos = false;
        $resultado           = false;

        //Insertar Datos Generales del Flujo
        $flujo_data = array(
            'flu_nombre'      => $data['nombre'],
            'flu_descripcion' => $data['descripcion'],
            'flu_estado'      => 1,
            'flu_id_tab'      => $data['tablas']
            );
        
        $this->db->insert( 'flu_flujo', $flujo_data );

        //Obtener ID de flujo
        $id_flujo = $this->db->insert_id();
        
        $id_tabla = $data['tablas'];

        if ( is_numeric( $id_flujo ) ) {
          $validacion_flujo = true;
      }

        //Insertar Campos
      foreach ( $data['fields'] as $campo ) {
        $detalles = explode("|", $campo);

        $campo_data =  array(
          'cam_id_tab' => $id_tabla,
          'cam_nombre' => $detalles[0],
          'cam_alias'  => $detalles[1],
          'cam_column' => ( $detalles[2] === "true" ) ? 1 : 0,
          'cam_field'  => ( $detalles[3] === "true" ) ? 1 : 0
          );
            //Insertar registro
        $this->db->insert( "cam_campo", $campo_data );
    }

    $validacion_campos = true;
    if ( isset( $data['relations'] )  && $data['relations'] != "" ) {
            //Insertar relaciones
        foreach ( $data['relations'] as $relacion ) {

            $detalles = explode( "|", $relacion );
            //Obtener el id del campo padre
            $id_campo_padre = $this->db
            ->get_where( "cam_campo", array( 'cam_nombre' => $detalles[0]) )
            ->row()->cam_id;
                //Solamente un alias de $id_tabla
            $id_tabla_padre = $id_tabla;
                //Nombre de la tabla hija
            $id_tabla_hija = $detalles[1];
                //Nombre del campo hijo
            $id_campo_hijo = $detalles[2];
                //Preparar datos para insercion
            $data_relacion = array(
                'rel_id_cam_padre' => $id_campo_padre,
                'rel_id_cam_hija'  => $id_campo_hijo,
                'rel_id_tab_padre' => $id_tabla_padre,
                'rel_id_tab_hija'  => $id_tabla_hija
                );
                // Be happy!
            $this->db->insert( "rel_relacion", $data_relacion );
        }
        $validacion_relacion = true;
    } else {
        $validacion_relacion = true;
    }


        //Insertar Roles, Estados y Permisos
    foreach ( $data['roles-estados'] as $roles_estados ) {

        $detalles = explode( "|", $roles_estados );
        $data_roles_estados = array(
          'exp_id_flu' => $id_flujo,
          'exp_id_est' => $detalles[0],
          'exp_orden' => $detalles[3],
          'exp_estado' => 1
          );

        $this->db->insert( 'exp_estadoxproceso', $data_roles_estados );
        $id_estado_proceso = $this->db->insert_id();
        $detalles_permisos = explode( ",", $detalles[2] );

            //Recorrer todos los permisos
        foreach ($detalles_permisos as $permiso) {
            $data_permiso_proceso = array(
                'pxp_id_exp' => $id_estado_proceso,
                'pxp_id_per' => $permiso,
                'pxp_id_rol' => $detalles[1]
                );
            
            $this->db->insert('pxp_permisoxprocesoxrol', $data_permiso_proceso);
        }
    }

    $validacion_permisos = true;

    if ( $validacion_flujo == true && $validacion_campos == true && $validacion_relacion = true && $validacion_permisos == true ) {
      $resultado = true;
  }

        //si es valido retornar id de flujo
  return ( $resultado == true )
  ? $id_flujo
  : false;
}

    /**
     * Verifica si el nombre del flujo es unico en la base de datos
     * @param $nombre nombre del flujo
     * @return string json response true or false
     */
    public function getValidacionNombreFlujo( $nombre )
    {
        $resultado  = $this->db->get_where( "flu_flujo", array("flu_nombre" => $nombre ) )
        ->num_rows();

        return ( $resultado == 0 )
        ? json_encode(true)
        : json_encode(false);
    }

    /**
     * Obtiene listado de flujos activos en el sistema
     * @return object listado de flujos activos en el sistema
     */
    public function getListadoFlujos()
    {
        $result = $this->db->get_where( 'flu_flujo', array( 'flu_estado' => 1 ) );
        return ( is_object( $result ) && $result->num_rows() > 0 )
        ? $result
        : false;
    }

    /**
     * Obtiene listado de flujos, elimina de la lista el ID
     * de flujo enviado por parametro
     * @param  integer $id_flujo ID de Flujo
     * @return json listado de flujos
     */
    public function getListadoFlujosWhereNot( $id_flujo = 0 )
    {
        $not_int_query = "SELECT [dbo].[flu_flujo].* 
        FROM [dbo].[flu_flujo] 
        WHERE [dbo].[flu_flujo].flu_id 
        NOT IN ( ? )";

        $result = $this->db->query( $not_int_query, array( $id_flujo ) );

        return ( is_object( $result ) && $result->num_rows() > 0 )
        ? json_encode( $result->result() )
        : false;
    }

    /**
     * Obtiene referencia de retorno (padre) y labels superiores 
     * @param  array   $info_flujo informacion del padre, nombre del flujo, registro de refencia, campo, etc.
     * @param  integer $id ID de registro padre
     * @return array label del referencia, ID del padre de segundo nivel
     */
    public function get_referencia( $info_flujo = array(), $id = 0 )
    {
        //Obtiendo referencia de padre 
        $padre[0]               = $this->get_registro($info_flujo['tab_nombre'],$id);
        $padre[0]['campo']      = $info_flujo['cam_alias'];
        $padre[0]['referencia'] = $padre[0][$info_flujo['cam_nombre']];       
        $i = 1;

        //Escalar en la jerarquia hasta encontrar el padre maestro
        while ( $info_flujo['flu_padre'] > 0 ) {
            $prefijo                 = explode( "_", $info_flujo['tab_nombre'] );
            $info_flujo              = $this->get_padre( $info_flujo['flu_padre'] );
            $sufijo                  = explode( "_", $info_flujo['tab_nombre'] );
            //Se construye el label de referencia
            $padre[$i]               = $this->get_registro( $info_flujo['tab_nombre'], $padre[$i-1][$prefijo[0] . '_id_' . $sufijo[0]] );
            $padre[$i]['campo']      = $info_flujo['cam_alias'];
            $padre[$i]['referencia'] = $padre[$i][$info_flujo['cam_nombre']];
            //Construye arreglo de IDS para seguir obtiendo el padre
            $padre[$i]['id']         = $padre[$i][$sufijo[0].'_id'];
            $i++;
        }
        //Obtener el ID de retorno, si el padre es un sub-flujo
        $referencia[1] = ( isset( $padre[1] ) ) ? $padre[1]['id'] : null;
        //Ordenar de manera inversa
        $padre         = array_reverse($padre);
        $referencia[0] = " - Referencia: ";
        //Construyendo label a mostrar
        foreach ( $padre as $key ) {
            $referencia[0] .= $key['campo']." ".$key['referencia']." ";
        }
        //Enviado array de referencia
        return $referencia;
    }

    /**
     * Obtiene campos relacionados con el flujo seleccionado
     * @param  integer $id_flujo ID de flujo
     * @return object listado de campos seleccionados
     */
    public function getCamposFlujo( $id_flujo = 0 )
    {
        $query_campos = "SELECT tab_nombre AS tabla, 
        cam_id AS id,
        cam_nombre AS nombre,
        cam_alias AS alias, 
        cam_column AS grid,
        cam_field AS agregar,
        cam_edit AS edit,
        cam_orden AS orden,
        flu_nombre as flujo
        FROM [dbo].[tab_tabla] AS tabla
        INNER JOIN [dbo].[cam_campo] AS campo ON campo.cam_id_tab = tabla.tab_id
        INNER JOIN [dbo].[flu_flujo] AS flujo ON flujo.flu_id_tab = tabla.tab_id
        WHERE flujo.flu_id = ?
        ORDER BY cam_orden";

        $resultado = $this->db->query( $query_campos, array( $id_flujo ) );

        return ( is_object( $resultado ) && $resultado->num_rows() > 0 )
        ? $resultado
        : false;
    }

    /**
     * Actualiza valores de los campos utilizados en el flujo seleccionado
     * @param  string  $campo nombre del campo a actualizar
     * @param  string  $value nuevo valor
     * @param  integer $id    id del campo
     * @return boolean resultado de la operacion
     */
    public function actualizarCampos( $tabla = "", $campo = "", $value = "", $where = "", $id = 0 )
    {
        $this->db->where( $where, $id );
        $resultado = $this->db->update($tabla, array( $campo => $value ) );
        return json_encode($resultado);
    }

    /**
     * Elimina campo del flujo seleccionado
     * @param  integer $campo ID del campo a eliminar
     * @return boolean resultado de la operacion
     */
    public function eliminarCampos( $campo = 0 )
    {
        return $this->db->delete( 'cam_campo', array( 'cam_id' => $campo ) );
    }

    /**
     * Obtiene listado de estados del flujo seleccionado
     * @param  integer $id_flujo ID del flujo seleccionado
     * @return object listado de estados
     */
    public function getListadoEstadosFlujo( $id_flujo = 0 )
    {
        $query_estados = "SELECT estadoxproceso.exp_id AS id,
        IIF(estadoxproceso.exp_descripcion IS NULL, 'No Definido', estadoxproceso.exp_descripcion) AS descripcion,
        estado.est_nombre AS nombre,
        IIF(estadoxproceso.exp_estado = 1, 'Activo', 'Inactivo') AS estado_string,
        estadoxproceso.exp_estado AS estado_int,
        estadoxproceso.exp_orden AS orden
        FROM [dbo].[exp_estadoxproceso] AS estadoxproceso 
        INNER JOIN [dbo].[est_estado] AS estado ON estado.est_id = estadoxproceso.exp_id_est
        WHERE estadoxproceso.exp_id_flu = ?";

        $resultado = $this->db->query( $query_estados, array( $id_flujo ) );
        return ( is_object( $resultado ) && $resultado->num_rows() > 0 )
        ? $resultado
        : false;
    }
}

