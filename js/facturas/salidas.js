var salidas = {
    fn: {
        regresarReponsable: function (event) {
            event.preventDefault();
            var that = $(this),
                datos = {
                    id: that.data('id')
                },
                url = gSateliteWhite.baseUrl('tamosa/facturas/salida/regresar');

            $.ajax({
                async: false,
                url: url,
                data: datos,
                dataType: "JSON",
                type: "POST",
                success: function (data) {
                    if (data === 1) {
                        that.parent().parent().remove();
                    }
                }
            });
        },
        copiarSeleccionados: function () {
            var elementos = $("input[name='pedidos[]']"),
                inputHidden = "",
                formElementos = $("#seleccionados");

            $.each(elementos, function (index, element) {
                if ($(element).is(":checked")) {
                    inputHidden += '<input type="hidden" name="facturas[]" value="' + $(element).val() + '">';
                }
            });
             formElementos.append(inputHidden);
           // return false;
        }
    },
    onReady: function () {
        $("a[name='regresar[]']").on("click", salidas.fn.regresarReponsable);
        $("#frm-salida").on("submit", salidas.fn.copiarSeleccionados);
    }
};

jQuery(document).ready(function($) {
    salidas.onReady();
});