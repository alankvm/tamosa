var lista = {
	fn: {
		copiarFacturasSeleccionadas: function (event) {
			event.preventDefault();
			var $divFacturasSeleccionadas  = $("form#facturas_seleccionadas"),
				facturas = '';
			if ($(this).is(":checked")) {
				$divFacturasSeleccionadas.prepend('<input type="hidden" name="seleccion[]" value="' + $(this).val() + '">');
			}
			
		}
	},
	onReady: function () {
		$("table").on("click", "input[name='facturas[]']", lista.fn.copiarFacturasSeleccionadas);
	},
	init: function () {
		lista.onReady();
	}
};
jQuery(document).ready(function($) {
	lista.init();
});