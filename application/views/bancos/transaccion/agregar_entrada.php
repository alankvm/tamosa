<form method="post">
    <div class="row-fluid">

        <div class="span12">
            <div class="box">

                <div class="title">

                    <h4>
                        <span class="icon16 icomoon-icon-equalizer-2"></span>
                        <span>Transaccion</span>
                    </h4>
                    <a href="#" class="minimize" style="display: none;">Minimize</a>
                </div>
                <div class="content" style="display: block;">

                    <div class="form-row row-fluid">
                        <div class="span12">
                            <div class="row-fluid">
                                <label class="form-label span8" for="checkboxes">Banco: <?php echo $cuenta['ban_nombre']?>   -   Cuenta: <?php echo $cuenta['cue_numero']?></label>                                
                            </div>
                        </div> 
                    </div>
                    <div class="form-row row-fluid">
                        <div class="span12">
                            <div class="row-fluid">
                                <label class="form-label span3" for="checkboxes">Monto de entrada</label>
                                <div class="span6 controls">   
                                    <input type="text" name="entrada" id="entrada" value="0">
                                </div> 
                            </div>
                        </div> 
                    </div>
                    <div class="form-row row-fluid">
                        <div class="span12">
                            <div class="row-fluid">
                                <label class="form-label span3" for="checkboxes">Numero de comprobante</label>
                                <div class="span6 controls">   
                                    <input type="text" name="comprobante" id="comprobante">
                                </div> 
                            </div>
                        </div> 
                    </div>
                    <div class="form-row row-fluid">
                        <div class="span12">
                            <div class="row-fluid">
                                <label class="form-label span3" for="checkboxes">Concepto</label>
                                <div class="span6 controls">   
                                    <textarea name="concepto" id="concepto"></textarea>
                                </div> 
                            </div>
                        </div> 
                    </div>
             </div>

         </div>         

</div><!-- End .span12 -->

</div><!-- End .row-fluid -->           
<div class="row-fluid">

    <div class="span12">

        <div class="box gradient">

            <div class="title">
                <h4>
                    <span>Facturas sin remesar</span>
                </h4>
            </div>
            <div class="content noPad clearfix">
                <table style="width:100%" border="1">
                    <thead>
                        <tr>
                            <th>No Factura</th>
                            <th>Monto</th>
                            <th>Agregar</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($facturas as $key) { ?>
                        <tr>
                            <td><?php echo $key['documento']?></td>
                            <td><?php echo $key['fac_monto']?></td>
                            <td><input type="checkbox" id="factura" name="factura[]" data-monto="<?php echo $key['fac_monto'] ?>" onchange="monto($(this))" value="<?php echo $key['fac_id']?>" class="ibuttonCheck nostyle" /></td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
            <br>
            <div class="form-actions">
             <button type="submit" class="btn btn-success" >Guardar</button>
             <button type="reset" class="btn btn-success" >Cancelar</button>
         </div>

     </div><!-- End .box -->

 </div><!-- End .span12 -->

</div><!-- End .row-fluid -->
</form>

<script type="text/javascript" src="<?php echo base_url()?>js/bancos/transaccion.js"></script>