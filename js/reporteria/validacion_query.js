var validacion = {
    fn: {
        validarNuevoReporte: function () {
            //Agregar reglas de validacion para el formulario de nuevo reporte
            $("form#nuevorpt").validate({
                rules: {
                    nombre: {
                        required: true,
                        minlength: 5
                    },
                    descripcion: {
                        required: true,
                        minlength: 10
                    }
                }
            });
        },
        verificarNuevoReporte: function () {
            var resultado = false,
                $dropDownTablas = $("select#tablas");

            //Verificar Nombre del Nuevo Reporte
            if ($("#nombre").valid() === 1) {
                resultado = true;
            } else {
                resultado = false;
            }

            //Verificar Descripcion del Nuevo Reporte
            if ($("#descripcion").valid() === 1) {
                resultado = true;
            } else {
                resultado = false;
            }

            //Restablecer seleccion en tablas 
            if (resultado === false) {
                $dropDownTablas.select2("data", null);
            }
            return resultado;
        },
        validarAgregarCampo: function () {
            //Agregar reglas de validacion para campos y alias
            $("form#frmagregar").validate({
                rules: {
                    alias: {
                        required: true,
                        alpha: true,
                        minlength: 3
                    }
                }
            });
        },
        verificarAgregarCampo: function () {
            var resultado = false;

            if ($("#alias").valid() === 1) {
                resultado = true;
            } else {
                resultado = false;
            }

            return resultado;
        },
        validarAgregarRelacion: function () {
            //Agregar reglas de validacion
            $("#frmforeign").validate({
                rules: {
                    foreign_alias: {
                        required: true,
                        alpha: true,
                        minlength: 3
                    }
                }
            });
        },
        verificarAgregarRelacion: function () {
            var resultado = false;

            if ($("#foreign_alias").valid() === 1) {
                resultado = true;
            } else {
                resultado = false;
            }

            return resultado;
        }
    },
    onReady: function () {
        //Paso 1
        validacion.fn.validarNuevoReporte();
        //Paso 2
        validacion.fn.validarAgregarCampo();
        //Paso 3
        validacion.fn.validarAgregarRelacion();
    },
    init: function () {
        validacion.onReady();
    }
};

jQuery(document).ready(function($) {
    $.validator.addMethod("alpha", function(value, element) {
        return this.optional(element) || value == value.match(/^[a-zA-Z]+$/);
    },"Solo caracteres alfab&eacute;ticos.");
    validacion.init();
   

});