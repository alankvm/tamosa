<?php

header("Content-Type: application/vnd.ms-excel");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("content-disposition: attachment;filename=" . $filename . ".xls");

?>

    <h3>
        <?php echo $detalles_reporte->que_nombre?>
    </h3>
    <p>
        <?php echo $detalles_reporte->que_descripcion?>
    </p>

    <table>
        <thead>
            <tr>
                <?=$table_header?>
            </tr>
        </thead>
        <tbody>
            <?php echo $table_tbody?>
        </tbody>
    </table>
</body>
