<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Procesos extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('proceso_model');
	}

	public function index()
	{
		//Verificar Permisos de acceso
        if ( !$this->tank_auth->is_logged_in() ) {
            redirect('/auth/login/');
        } else {
        	//$this->output->enable_profiler();
            $data['listado_tablas']   = $this->proceso_model->getListadoTablas();
            $data['listado_permisos'] = $this->proceso_model->getListadoPermisos();
            $data['listado_estados']  = $this->proceso_model->getEstados();
            $data['listado_roles']    = $this->proceso_model->getRoles();
        	$this->_cargarvista( null, $data );
        }
	}

    public function subflujo()
    {
        //Verificar Permisos de acceso
        if ( !$this->tank_auth->is_logged_in() ) {
            redirect('/auth/login/');
        } else {
            //$this->output->enable_profiler();
            $data['listado_flujos_padre'] = $this->proceso_model->getListadoFlujos();
            $data['listado_flujos_hijo'] = $this->proceso_model->getListadoFlujos();
            $data['listado_roles'] = $this->proceso_model->getRoles();
            $this->_cargarvista( null, $data );
        }
    }
	public function _cargarvista($data=0,$crud=0)
    {   
        $this->load->view('vacia',$crud);   
        if($data!=0)
            $data=array_merge($data,$this->masterpage->getUsuario());
        else
            $data=$this->masterpage->getUsuario();
        $vista=$data['modulo'].'/'.$data['control'].'/'.$data['funcion'];
        $this->masterpage->setMasterPage('masterpage_default');
        $this->masterpage->addContentPage($vista, 'content',$data);
        $this->masterpage->show();
    }

    /**
     * Obtiene columnas pertenecientes a una tabla en especifico
     */
    public function getColumnas()
    {
        //Verificar acceso unicamente, mediante AJAX
        if ( !$this->input->is_ajax_request() ) {
            exit('No direct script access allowed');
        }

        $nombre_tabla = $this->input->get('tabla');
        $result = $this->proceso_model->getColumnas( $nombre_tabla );
        echo $result;
    }

    /**
     * Obtiene relaciones asociadas a una tabla en especifico
     */
    public function getRelaciones()
    {
        //Verificar acceso unicamente, mediante AJAX
        if ( !$this->input->is_ajax_request() ) {
            exit('No direct script access allowed');
        }   

        $nombre_tabla = $this->input->get('tabla');
        $result = $this->proceso_model->getRelaciones( $nombre_tabla );
        echo $result;
    }

    /**
     * Creacion de nuevo proceso
     */
    public function nuevo()
    {
        $data = $this->input->post();
        $resultado = $this->proceso_model->setNuevoProceso( $data );

        if ( $resultado ) {
            redirect(base_url("sistema/procesos"));
        }
    }

    /**
     * Despliega lista de procesos activos en el sistema
     */
    public function mantenimiento()
    {
        //Verificar Permisos de acceso
        if ( !$this->tank_auth->is_logged_in() ) {
            redirect('/auth/login/');
        } else {
            $this->output->enable_profiler();
            $data['listado_flujos'] = $this->proceso_model->getListadoFlujos();
            $this->_cargarvista( null, $data );
        }
    }

    /**
     * Despliega lista de campos pertenecientes a un proceso en especifico
     * @param  integer $id_flujo ID de flujo
     */
    public function editar( $id_flujo )
    {
        //Verificar Permisos de acceso
        if ( !$this->tank_auth->is_logged_in() ) {
            redirect('/auth/login/');
        } else {
            $this->output->enable_profiler();
            $data['listado_campos']     = $this->proceso_model->getCamposFlujo( $id_flujo );
            $data['listado_estados']    = $this->proceso_model->getListadoEstadosFlujo( $id_flujo );
            $this->_cargarvista( null, $data );
        }
    }

    /**
     * Actualiza datos de campos para el proceso seleccinado
     * @return json resultado de la operacion
     */
    public function actualizar()
    {
        //Verificar acceso unicamente, mediante AJAX
        if ( !$this->input->is_ajax_request() ) {
            exit('No direct script access allowed');
        }

        $tabla = $this->input->post("tabla");
        $campo = $this->input->post("campo");
        $value = $this->input->post("value");
        $where = $this->input->post("where");
        $id    = $this->input->post("id");
        
        $resultado = $this->proceso_model->actualizarCampos( $tabla, $campo, $value, $where, $id );
        echo $resultado;
    }

    public function eliminar_campo()
    {
        //Verificar acceso unicamente, mediante AJAX
        if ( !$this->input->is_ajax_request() ) {
            exit('No direct script access allowed');
        }

        $campo = $this->input->post("campo");
        
        $resultado = $this->proceso_model->eliminarCampos( $campo );
        echo $resultado;
    }

    /**
     * Verifica si el nombre del flujo es Unico, invocado desde js/form-proceso.js
     */
    public function esUnico()
    {
        //Verificar acceso unicamente, mediante AJAX
        if ( !$this->input->is_ajax_request() ) {
            exit('No direct script access allowed');
        }

        $nombre = trim( $this->input->get("nombre") );
        $resultado = $this->proceso_model->getValidacionNombreFlujo( $nombre );
        echo $resultado;
    }

    public function getSubflujos()
    {
        //Verificar acceso unicamente, mediante AJAX
        if ( !$this->input->is_ajax_request() ) {
            exit('No direct script access allowed');
        }

        $id_flujo = trim( $this->input->get("id_flujo") );
        $resultado = $this->proceso_model->getListadoFlujosWhereNot( $id_flujo );
        echo $resultado;
    }
}

/* End of file procesos.php */
/* Location: ./application/controllers/procesos.php */